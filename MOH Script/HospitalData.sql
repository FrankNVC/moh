﻿USE [MOH]
GO

INSERT INTO [dbo].[Hospitals]
           ([Name]
           ,[LicenseNo]
           ,[Address])
     VALUES
           ('An Binh',
           'H11111',
           '146 AN BINH, P.7, Q.5, TP. HCM')

INSERT INTO [dbo].[Hospitals]
           ([Name]
           ,[LicenseNo]
           ,[Address])
     VALUES
           ('Binh Dan',
           'H11112',
           '371 ĐIEN BIEN PHU, P.4, Q.3, TP. HCM')

INSERT INTO [dbo].[Hospitals]
           ([Name]
           ,[LicenseNo]
           ,[Address])
     VALUES
           ('Nhi Dong 1',
           'H11113',
           '327 Ly Thai To')

INSERT INTO [dbo].[Hospitals]
           ([Name]
           ,[LicenseNo]
           ,[Address])
     VALUES
           ('Nhan Dan',
           'H12345',
           '88 THANH THAI, P.12, Q.10, TP. HCM')

INSERT INTO [dbo].[Hospitals]
           ([Name]
           ,[LicenseNo]
           ,[Address])
     VALUES
           ('Chan Thuong Chinh Hinh',
           'H11114',
           '929 TRAN HUNG ĐAO, P.1, Q.5, TP. HCM')

INSERT INTO [dbo].[Hospitals]
           ([Name]
           ,[LicenseNo]
           ,[Address])
     VALUES
           ('30 Thang 4',
           'H11115',
           '9 SU VAN HANH, P.9, Q.5, TP. HCM')

INSERT INTO [dbo].[Hospitals]
           ([Name]
           ,[LicenseNo]
           ,[Address])
     VALUES
           ('Pham Ngoc Thach',
           'H12568',
           '120 HUNG VUONG, P.12, Q.5, TP. HCM')
GO