USE [MOH]
GO

INSERT INTO [dbo].[Doctors]
           ([Name]
           ,[Gender]
           ,[DOB]
           ,[LicenseNo]
           ,[Address])
     VALUES
           ('La Minh Luong','Male', '01-01-1981','D11111','312 Ly Chinh Thang')

INSERT INTO [dbo].[Doctors]
           ([Name]
           ,[Gender]
           ,[DOB]
           ,[LicenseNo]
           ,[Address])
     VALUES
           ('Tran Manh Tra','Male', '02-02-1982','D11112','10 Phu Dong Thien Vuong')

INSERT INTO [dbo].[Doctors]
           ([Name]
           ,[Gender]
           ,[DOB]
           ,[LicenseNo]
           ,[Address])
     VALUES
           ('Nguyen Nhu Huan','Male', '03-03-1983','D11113','34/6A Ngo Gia TU')

INSERT INTO [dbo].[Doctors]
           ([Name]
           ,[Gender]
           ,[DOB]
           ,[LicenseNo]
           ,[Address])
     VALUES
           ('Truong Thi Mai Trang','Female', '04-04-1984','D11114','452 Dien Bien Phu')

INSERT INTO [dbo].[Doctors]
           ([Name]
           ,[Gender]
           ,[DOB]
           ,[LicenseNo]
           ,[Address])
     VALUES
           ('Ngo Thanh Thao','Female', '05-05-1985','D11115','12 Ly Thai To')
GO

