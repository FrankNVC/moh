Nguyen Viet Cuong
s3393346
The assignment made use of the following features
- ASP.NET, ASP.NET AJAX, AJAX Control ToolKit, ADO.NET, SQL-Server, Stored Procedure, Web Service and QUARZT.NET
- ASP.NET seem easier to use but using much more resources, there are some errors and bugs with the Visual Studio
- STORED PROCEDURE is complicated and required ORM, otherwise it only work directly with the database and not with the
object-orriented-programming model

Run the ERM.sql file to create database.
Run the following sql to create table in order: Doctors, Patients, Hospitals, DrugGroups, Drugs, ICDGroups, ICDs, MedicalServiceGroups, MedicalServices,
LabOrders, LabOrderDetails, Prescriptions, PrescriptionDetails, Visits
Run the sql Data with the same order to populate data
Run all the sp sql to create Stored Procedure.

Repeat with MOH.sql to create and populate database.

Instruction:
	- Run the project with Default.aspx
	- Admin: admin / admin@111
	- Member: member / member@111
The AutoUpdate service will run after invoking by clicking Update button on Update.aspx

REFERENCE:
css: http://indeziner.com/resources/freebies/free-css-templates/free-htmlcss-jquery-admin-panel-inadmin/
healthcarebluebook.com
http://apps.who.int/classifications/icd10/browse/2010/en#/I
http://geekswithblogs.net/TarunArora/archive/2013/01/20/quartz.net-scheduler-exposed-via-a-web-service.aspx