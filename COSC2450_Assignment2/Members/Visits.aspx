﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Visits.aspx.cs" Inherits="COSC2450_Assignment2.Visits2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="title">
        <h2>This is the Visit Page.</h2>
    </div>
    <cc1:Accordion ID="Accordion1" runat="server" HeaderCssClass="accHeader" HeaderSelectedCssClass="accHeader" ContentCssClass="accContent">
        <Panes>
            <cc1:AccordionPane ID="AccordionPane1" runat="server">
                <Header> View All Visits</Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gridVisits" runat="server" CellSpacing="2" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="ID"
                                ForeColor="#333333" GridLines="None" DataKeyName="ID" Width="600px" OnPageIndexChanging="gridVisits_PageIndexChanging"
                                OnSelectedIndexChanging="gridVisits_SelectedIndexChanging">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <Columns>
                                    <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID" />
                                    <asp:BoundField DataField="Date" HeaderText="Date" ReadOnly="True" SortExpression="Date" DataFormatString="{0:MM/dd/yyyy}" />
                                    <asp:BoundField DataField="PatientID" HeaderText="PatientID" ReadOnly="True" SortExpression="PatientID" />
                                    <asp:BoundField DataField="HospitalID" HeaderText="HospitalID" ReadOnly="True" SortExpression="HospitalID" />
                                    <asp:BoundField DataField="DoctorID" HeaderText="DoctorID" ReadOnly="True" SortExpression="DoctorID" />
                                    <asp:BoundField DataField="PrescriptionID" HeaderText="PrescriptionID" ReadOnly="True" SortExpression="PrescriptionID" />
                                    <asp:BoundField DataField="LabOrderID" HeaderText="LabOrderID" ReadOnly="True" SortExpression="LabOrderID" />
                                    <asp:BoundField DataField="ICDID" HeaderText="ICDID" ReadOnly="True" SortExpression="ICDID" />
                                    <asp:BoundField DataField="Outcome" HeaderText="Outcome" ReadOnly="True" SortExpression="Outcome" />
                                    <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                                </Columns>
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <PagerStyle BackColor="#284755" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="true" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284755" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                            <asp:Panel ID="pnlVisit" runat="server">
                                <div>
                                    <h2>Visit</h2>
                                </div>
                                <div class="form">
                                    <fieldset>
                                        <dl>
                                            <dt>
                                                <label>ID:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtIDView" runat="server" size="5"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Date:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtDateView" runat="server" size="20"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Patient Name:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtPatientView" runat="server" size="30"></asp:TextBox>
                                            </dd>
                                        </dl>
                                         <dl>
                                            <dt>
                                                <label>Hospital Name:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtHospitalView" runat="server" size="30"></asp:TextBox>
                                            </dd>
                                        </dl>
                                         <dl>
                                            <dt>
                                                <label>Doctor Name:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtDoctorView" runat="server" size="30"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>ICD Diagnosis:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtICDView" runat="server" size="50"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Outcome:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtOutcomeView" runat="server" size="10"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gridPrescriptionDetails" runat="server" CellSpacing="2" AutoGenerateColumns="False" AllowSorting="True" DataKeyNames="ID"
                                                        ForeColor="#333333" GridLines="None" DataKeyName="ID" Width="600px" OnSelectedIndexChanging="gridPrescriptionDetails_SelectedIndexChanging">
                                                        <Columns>
                                                            <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID" />
                                                            <asp:TemplateField HeaderText="Drug">
                                                                <ItemTemplate>
                                                                    <%# Eval("DrugID") %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Prescription ID">
                                                                <ItemTemplate>
                                                                    <%# Eval("PrescriptionID") %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Quantity">
                                                                <ItemTemplate>
                                                                    <%# Eval("Quantity") %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Dose">
                                                                <ItemTemplate>
                                                                    <%# Eval("Dose") %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Instruction">
                                                                <ItemTemplate>
                                                                    <%# Eval("Instruction") %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                                                        </Columns>
                                                        <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                                        <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                                        <PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
                                                        <RowStyle BackColor="#EFF3FB"></RowStyle>
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB"></SortedAscendingCellStyle>
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1"></SortedAscendingHeaderStyle>
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF"></SortedDescendingCellStyle>
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE"></SortedDescendingHeaderStyle>
                                                    </asp:GridView>
                                                    <asp:Panel ID="pnlPrescriptionDetail" runat="server">
                                                        <div>
                                                            <h2>Prescription Detail</h2>
                                                        </div>
                                                        <div class="form">
                                                            <fieldset>
                                                                <dl>
                                                                    <dt>
                                                                        <label>ID:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtPrescriptionIDView" runat="server" size="5"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                                <dl>
                                                                    <dt>
                                                                        <label>Date:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtPrescriptionDateView" runat="server" size="20"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                                <dl>
                                                                    <dt>
                                                                        <label>Drug:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtPrescriptionDrugView" runat="server" size="50"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                                <dl>
                                                                    <dt>
                                                                        <label>Quantity:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtPrescriptionQuantityView" runat="server" size="5"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                                <dl>
                                                                    <dt>
                                                                        <label>Dose:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtPrescriptionDoseView" runat="server" size="5"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                                <dl>
                                                                    <dt>
                                                                        <label>Instruction:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtPrescriptionInstructionView" runat="server"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                            </fieldset>
                                                        </div>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </dl>
                                        <dl>
                                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gridLabOrderDetails" runat="server" CellSpacing="2" AutoGenerateColumns="False" AllowSorting="True" DataKeyNames="ID"
                                                        ForeColor="#333333" GridLines="None" DataKeyName="ID" Width="600px" OnSelectedIndexChanging="gridLabOrderDetails_SelectedIndexChanging">
                                                        <Columns>
                                                            <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID" />
                                                            <asp:TemplateField HeaderText="Medical Service">
                                                                <ItemTemplate>
                                                                    <%# Eval("MedicalServiceID") %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Result">
                                                                <ItemTemplate>
                                                                    <%# Eval("Result") %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Lab Order ID">
                                                                <ItemTemplate>
                                                                    <%# Eval("LabOrderID") %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                                                        </Columns>
                                                        <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                                        <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                                        <PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
                                                        <RowStyle BackColor="#EFF3FB"></RowStyle>
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB"></SortedAscendingCellStyle>
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1"></SortedAscendingHeaderStyle>
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF"></SortedDescendingCellStyle>
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE"></SortedDescendingHeaderStyle>
                                                    </asp:GridView>
                                                    <asp:Panel ID="pnlLabOrder" runat="server">
                                                        <div>
                                                            <h2>Lab Order Detail</h2>
                                                        </div>
                                                        <div class="form">
                                                            <fieldset>
                                                                <dl>
                                                                    <dt>
                                                                        <label>ID:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtLabOrderIDView" runat="server" size="5"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                                <dl>
                                                                    <dt>
                                                                        <label>Date:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtLabOrderDateView" runat="server"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                                <dl>
                                                                    <dt>
                                                                        <label>Medical Service:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtLabOrderMedView" runat="server" size="50"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                                <dl>
                                                                    <dt>
                                                                        <label>Result:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtLabOrderResultView" runat="server" size="10"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                            </fieldset>
                                                        </div>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <asp:DropDownList ID="dropVisit" runat="server" AutoPostBack="True"></asp:DropDownList>
                                                <cc1:ListSearchExtender ID="dropVisit_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropVisit" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                            </dt>                                            
                                        </dl>
                                        <dl>
                                            <dt></dt>
                                            <dd>
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                                            </dd>
                                        </dl>
                                    </fieldset>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlSearch" runat="server">
                                <asp:TextBox ID="txtDateSearch" runat="server" Text="Date"></asp:TextBox>
                                <asp:DropDownList ID="dropPatientSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropPatientSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropPatientSearch" IsSorted="true" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:DropDownList ID="dropHospitalSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropHospitalSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropHospitalSearch" IsSorted="true" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:DropDownList ID="dropDoctorSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropDoctorSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropDoctorSearch" IsSorted="true" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:DropDownList ID="dropPrescriptionSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropPrescriptionSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropPrescriptionSearch" IsSorted="true" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:DropDownList ID="dropLabOrderSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropLabOrder_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropLabOrderSearch" IsSorted="true" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:DropDownList ID="dropICDSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropICDSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropICDSearch" IsSorted="true" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:DropDownList ID="dropOutcomeSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropOutcomeSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropOutcomeSearch" IsSorted="true" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:Button ID="btnSearch2" runat="server" Text="Search Many" OnClick="btnSearch2_Click" />
                            </asp:Panel>
                            <asp:GridView ID="gridVisits2" runat="server" CellSpacing="2" DataKeyNames="ID"
                                ForeColor="#333333" GridLines="None" Width="600px">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <PagerStyle BackColor="#284755" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="true" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284755" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </cc1:AccordionPane>
        </Panes>
    </cc1:Accordion>
    <asp:SqlDataSource ID="sourcePatients" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM Patients"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sourcePrescriptions" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM Prescriptions"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sourceLabOrders" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM LabOrders"></asp:SqlDataSource>
</asp:Content>
