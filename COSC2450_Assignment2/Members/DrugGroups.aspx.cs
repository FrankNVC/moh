﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data.SqlClient;

namespace COSC2450_Assignment2
{
    public partial class DrugGroups2 : System.Web.UI.Page
    {
        private string conStr = WebConfigurationManager.ConnectionStrings["SqlService"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateData();
            }
        }

        protected void gridDrugGroups_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridDrugGroups.PageIndex = e.NewPageIndex;
            PopulateData();
        }        

        private void PopulateData()
        {
            dropDrugGroup.Items.Clear();

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectDrugGroups";
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                ad.Fill(table);
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                gridDrugGroups.DataSource = table;
                gridDrugGroups.DataBind();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectDrugGroups";
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = reader["ID"] + ", " + reader["Name"];
                    newItem.Value = reader["ID"].ToString();
                    dropDrugGroup.Items.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectDrugGroup1";
            cmd.Parameters.AddWithValue("@ID", dropDrugGroup.SelectedValue);
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                txtIDView.Text = reader["ID"].ToString();
                txtNameView.Text = reader["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }
        }

        protected void gridDrugGroups_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            var ID = gridDrugGroups.DataKeys[e.NewSelectedIndex].Value;            

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectDrugGroup1";
            cmd.Parameters.AddWithValue("@ID", ID);
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                txtIDView.Text = reader["ID"].ToString();
                txtNameView.Text = reader["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }
        }

        protected void btnSearch2_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSearchDrugGroup";
            cmd.Parameters.AddWithValue("@Name", txtNameSearch.Text);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                ad.Fill(table);
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            gridDrugGroups2.DataSource = table;
            gridDrugGroups2.DataBind();
            PopulateData();
        }
    }
}