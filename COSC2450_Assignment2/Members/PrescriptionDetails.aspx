﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="PrescriptionDetails.aspx.cs" Inherits="COSC2450_Assignment2.PrescriptionDetails2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="title">
        <h2>This is the Prescription Detail Page.</h2>
    </div>
    <cc1:Accordion ID="Accordion1" runat="server" HeaderCssClass="accHeader" HeaderSelectedCssClass="accHeader" ContentCssClass="accContent">
        <Panes>
            <cc1:AccordionPane ID="AccordionPane1" runat="server">
                <Header> View All Prescription Details</Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gridPrescriptionDetails" runat="server" CellSpacing="2" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="ID"
                                ForeColor="#333333" GridLines="None" DataKeyName="ID" Width="600px" OnPageIndexChanging="gridPrescriptionDetails_PageIndexChanging"
                                OnSelectedIndexChanging="gridPrescriptionDetails_SelectedIndexChanging">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <Columns>
                                    <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID" />
                                    <asp:BoundField DataField="DrugID" HeaderText="DrugID" ReadOnly="True" SortExpression="DrugID" />
                                    <asp:BoundField DataField="PrescriptionID" HeaderText="PrescriptionID" ReadOnly="True" SortExpression="PrescriptionID" />
                                    <asp:BoundField DataField="Quantity" HeaderText="Quantity" ReadOnly="True" SortExpression="Quantity" />
                                    <asp:BoundField DataField="Dose" HeaderText="Dose" ReadOnly="True" SortExpression="Dose" />
                                   <asp:BoundField DataField="Instruction" HeaderText="Instruction" ReadOnly="True" SortExpression="Instruction" />
                                    <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                                </Columns>
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <PagerStyle BackColor="#284755" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="true" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284755" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                            <asp:Panel ID="pnlPrescriptionDetail" runat="server">
                                <div>
                                    <h2>Prescription Detail</h2>
                                </div>
                                <div class="form">
                                    <fieldset>
                                        <dl>
                                            <dt>
                                                <label>ID:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtIDView" runat="server" size="5"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Date:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtDateView" runat="server" size="20"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Drug:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtDrugView" runat="server" size="30"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Quantity:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtQuantityView" runat="server" size="5"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Dose:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtDoseView" runat="server" size="5"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Instruction:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtInstructionView" runat="server" size="50"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <asp:DropDownList ID="dropPrescriptionDetail" runat="server" AutoPostBack="True" DataTextField="Name" DataValueField="ID"></asp:DropDownList></dt>
                                                <cc1:ListSearchExtender ID="dropPrescriptionDetail_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropPrescriptionDetail" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                        </dl>
                                        <dl>
                                            <dt></dt>
                                            <dd>                                                
                                                 <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" /><br />
                                            </dd>
                                        </dl>
                                    </fieldset>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlSearch" runat="server">                                
                                <asp:DropDownList ID="dropDrugSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropDrugSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropDrugSearch" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:DropDownList ID="dropPrescriptionIDSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropPrescriptionIDSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropPrescriptionIDSearch" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:TextBox ID="txtQuantitySearch" runat="server" Text="Quantity"></asp:TextBox>
                                <asp:TextBox ID="txtDoseSearch" runat="server" Text="Dose"></asp:TextBox>
                                <asp:Button ID="btnSearch2" runat="server" Text="Search Many" OnClick="btnSearch2_Click" />
                            </asp:Panel>
                            <asp:GridView ID="gridPrescriptionDetails2" runat="server" CellSpacing="2" DataKeyNames="ID"
                                ForeColor="#333333" GridLines="None" Width="600px">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <PagerStyle BackColor="#284755" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="true" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284755" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </cc1:AccordionPane>
        </Panes>
    </cc1:Accordion>
    <asp:SqlDataSource ID="sourcePrescriptionDetails" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM PrescriptionDetails"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sourcePrescriptionIDs" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM Prescriptions"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sourceDrugs" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM Drugs"></asp:SqlDataSource>
</asp:Content>