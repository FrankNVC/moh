﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data.SqlClient;

namespace COSC2450_Assignment2
{
    public partial class Doctors2 : System.Web.UI.Page
    {
        private string conStr = WebConfigurationManager.ConnectionStrings["SqlService"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateData();
            }
        }

        protected void gridDoctors_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridDoctors.PageIndex = e.NewPageIndex;
            PopulateData();
        }

        private void PopulateData()
        {
            dropDoctor.Items.Clear();
            dropGenderSearch.Items.Clear();

            dropGenderSearch.Items.Add("0");

            dropGenderSearch.Items.Add("Male");
            dropGenderSearch.Items.Add("Female");

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectDoctors";
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                ad.Fill(table);
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            gridDoctors.DataSource = table;
            gridDoctors.DataBind();

            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = reader["ID"] + ", " + reader["Name"];
                    newItem.Value = reader["ID"].ToString();
                    dropDoctor.Items.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally 
            {
                con.Close();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectDoctor1";
            cmd.Parameters.AddWithValue("@ID", dropDoctor.SelectedValue);
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                txtIDView.Text = reader["ID"].ToString();
                txtNameView.Text = reader["Name"].ToString();
                txtDateView.Text = reader["DOB"].ToString();
                txtLicenseView.Text = reader["LicenseNo"].ToString();
                txtAddressView.Text = reader["Address"].ToString();
                txtGenderView.Text = reader["Gender"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }
        }        

        protected void gridDoctors_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            var ID = gridDoctors.DataKeys[e.NewSelectedIndex].Value; 

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectDoctor1";
            cmd.Parameters.AddWithValue("@ID", ID);
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                txtIDView.Text = reader["ID"].ToString();
                txtNameView.Text = reader["Name"].ToString();
                txtDateView.Text = reader["DOB"].ToString();
                txtLicenseView.Text = reader["LicenseNo"].ToString();
                txtAddressView.Text = reader["Address"].ToString();
                txtGenderView.Text = reader["Gender"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }
        }

        protected void btnSearch2_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSearchDoctor";
            cmd.Parameters.AddWithValue("@Name", txtNameSearch.Text.Trim());
            cmd.Parameters.AddWithValue("@Gender", dropGenderSearch.SelectedValue);
            cmd.Parameters.AddWithValue("@DOB", txtDOBSearch.Text.Trim());
            cmd.Parameters.AddWithValue("@LicenseNo", txtLicenseNoSearch.Text.Trim());
            cmd.Parameters.AddWithValue("@Address", txtAddressSearch.Text.Trim());
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                ad.Fill(table);
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            gridDoctors2.DataSource = table;
            gridDoctors2.DataBind();
            PopulateData();
        }
    }
}