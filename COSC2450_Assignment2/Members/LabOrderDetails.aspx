﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="LabOrderDetails.aspx.cs" Inherits="COSC2450_Assignment2.LabOrderDetails2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="title">
        <h2>This is the Lab Order Detail Page.</h2>
    </div>
    <cc1:Accordion ID="Accordion1" runat="server" HeaderCssClass="accHeader" HeaderSelectedCssClass="accHeader" ContentCssClass="accContent">
        <Panes>
            <cc1:AccordionPane ID="AccordionPane1" runat="server">
                <Header> View All Lab Order Details</Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gridLabOrderDetails" runat="server" CellSpacing="2" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="ID"
                                ForeColor="#333333" GridLines="None" DataKeyName="ID" Width="600px" OnPageIndexChanging="gridLabOrderDetails_PageIndexChanging"
                                OnSelectedIndexChanging="gridLabOrderDetails_SelectedIndexChanging">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <Columns>
                                    <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID" />
                                    <asp:BoundField DataField="MedicalServiceID" HeaderText="MedicalServiceID" ReadOnly="True" SortExpression="MedicalServiceID" />
                                    <asp:BoundField DataField="Result" HeaderText="Result" ReadOnly="True" SortExpression="Result" />
                                    <asp:BoundField DataField="LabOrderID" HeaderText="LabOrderID" ReadOnly="True" SortExpression="LabOrderID" />                                    
                                    <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                                </Columns>
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <PagerStyle BackColor="#284755" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="true" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284755" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                            <asp:Panel ID="pnlLabOrderDetail" runat="server">
                                <div>
                                    <h2>Lab Order Detail</h2>
                                </div>
                                <div class="form">
                                    <fieldset>
                                        <dl>
                                            <dt>
                                                <label>ID:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtIDView" runat="server" size="5"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Date:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtDateView" runat="server"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Doctor Ordered:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtDoctorView" runat="server" size="30"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Medical Service:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtMedicalServiceView" runat="server" size="30"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Result:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtResultView" runat="server" size="30"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <asp:DropDownList ID="dropLabOrderDetail" runat="server" AutoPostBack="True"></asp:DropDownList>
                                                <cc1:ListSearchExtender ID="dropLabOrderDetail_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropLabOrderDetail" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                            </dt>                                            
                                        </dl>
                                        <dl>
                                            <dt></dt>
                                            <dd>
                                                <asp:Button ID="Button1" runat="server" Text="Search" OnClick="btnSearch_Click" />
                                            </dd>
                                        </dl>
                                    </fieldset>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlSearch" runat="server">                                
                                <asp:DropDownList ID="dropMedicalServiceSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="MedicalServiceSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropMedicalServiceSearch" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:TextBox ID="txtResultSearch" runat="server" Text="Result"></asp:TextBox>
                                <asp:DropDownList ID="dropLabOrderIDSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropLabOrderIDSearch_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropLabOrderIDSearch" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:Button ID="btnSearch2" runat="server" Text="Search Many" OnClick="btnSearch2_Click" />
                            </asp:Panel>
                            <asp:GridView ID="gridLabOrderDetails2" runat="server" CellSpacing="2" DataKeyNames="ID"
                                ForeColor="#333333" GridLines="None" Width="600px">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <PagerStyle BackColor="#284755" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="true" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284755" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </cc1:AccordionPane>
        </Panes>
    </cc1:Accordion>
    <asp:SqlDataSource ID="sourceLabOrderDetails" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM LabOrderDetails"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sourceLabOrders" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM LabOrders"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sourceMedicalServices" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT *  FROM MedicalServices"></asp:SqlDataSource>
</asp:Content>
