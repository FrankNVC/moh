﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data.SqlClient;

namespace COSC2450_Assignment2
{
    public partial class Visits2 : System.Web.UI.Page
    {
        private string conStr = WebConfigurationManager.ConnectionStrings["SqlService"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateData();
            }
        }

        protected void gridVisits_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridVisits.PageIndex = e.NewPageIndex;
            PopulateData();
        }

        private void PopulateData()
        {
            dropVisit.Items.Clear();
            dropPatientSearch.Items.Clear();
            dropHospitalSearch.Items.Clear();
            dropDoctorSearch.Items.Clear();
            dropPrescriptionSearch.Items.Clear();
            dropLabOrderSearch.Items.Clear();
            dropICDSearch.Items.Clear();
            dropOutcomeSearch.Items.Clear();

            dropPatientSearch.Items.Add("0");
            dropHospitalSearch.Items.Add("0");
            dropDoctorSearch.Items.Add("0");
            dropPrescriptionSearch.Items.Add("0");
            dropLabOrderSearch.Items.Add("0");
            dropICDSearch.Items.Add("0");
            dropOutcomeSearch.Items.Add("0");

            dropOutcomeSearch.Items.Add("CURED");
            dropOutcomeSearch.Items.Add("DECREASED");
            dropOutcomeSearch.Items.Add("INCREASED");
            dropOutcomeSearch.Items.Add("UNCHANGED");
            dropOutcomeSearch.Items.Add("DIED");

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectVisits";
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                ad.Fill(table);
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            gridVisits.DataSource = table;
            gridVisits.DataBind();

            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = reader["ID"] + ", " + reader["Date"];
                    newItem.Value = reader["ID"].ToString();
                    dropVisit.Items.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectHospitals";
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = reader["ID"] + ", " + reader["Name"];
                    newItem.Value = reader["ID"].ToString();
                    dropHospitalSearch.Items.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectDoctors";
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = reader["ID"] + ", " + reader["Name"];
                    newItem.Value = reader["ID"].ToString();
                    dropDoctorSearch.Items.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectPrescriptions";
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = reader["ID"] + ", " + reader["Date"];
                    newItem.Value = reader["ID"].ToString();
                    dropPrescriptionSearch.Items.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectLabOrders";
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = reader["ID"] + ", " + reader["Date"];
                    newItem.Value = reader["ID"].ToString();
                    dropLabOrderSearch.Items.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectPatients";
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = reader["ID"] + ", " + reader["Name"];
                    newItem.Value = reader["ID"].ToString();
                    dropPatientSearch.Items.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectICDs";
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = reader["ID"] + ", " + reader["Name"];
                    newItem.Value = reader["ID"].ToString();
                    dropICDSearch.Items.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string PatientID = "0";
            string HospitalID = "0";
            string DoctorID = "0";
            string PrescriptionID = "0";
            string LabOrderID = "0";
            string ICDID = "0";

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectVisit1";
            cmd.Parameters.AddWithValue("@ID", dropVisit.SelectedValue);
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                txtIDView.Text = reader["ID"].ToString();
                txtDateView.Text = reader["Date"].ToString();
                txtOutcomeView.Text = reader["Outcome"].ToString();
                PatientID = reader["PatientID"].ToString();
                HospitalID = reader["HospitalID"].ToString();
                DoctorID = reader["DoctorID"].ToString();
                PrescriptionID = reader["PrescriptionID"].ToString();
                LabOrderID = reader["LabOrderID"].ToString();
                ICDID = reader["ICDID"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectPatient1";
            cmd.Parameters.AddWithValue("@ID", PatientID);
            SqlDataReader reader2;
            try
            {
                con.Open();
                reader2 = cmd.ExecuteReader();
                reader2.Read();
                txtPatientView.Text = reader2["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectHospital1";
            cmd.Parameters.AddWithValue("@ID", HospitalID);
            SqlDataReader reader3;
            try
            {
                con.Open();
                reader3 = cmd.ExecuteReader();
                reader3.Read();
                txtHospitalView.Text = reader3["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectDoctor1";
            cmd.Parameters.AddWithValue("@ID", DoctorID);
            SqlDataReader reader4;
            try
            {
                con.Open();
                reader4 = cmd.ExecuteReader();
                reader4.Read();
                txtHospitalView.Text = reader4["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectICD1";
            cmd.Parameters.AddWithValue("@ID", ICDID);
            SqlDataReader reader5;
            try
            {
                con.Open();
                reader5 = cmd.ExecuteReader();
                reader5.Read();
                txtICDView.Text = reader5["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectPrescriptionDetail2";
            cmd.Parameters.AddWithValue("@PrescriptionID", PrescriptionID);
            DataTable table = new DataTable();
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                ad.Fill(table);
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                gridPrescriptionDetails.DataSource = table;
                gridPrescriptionDetails.DataBind();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectLabOrderDetail2";
            cmd.Parameters.AddWithValue("@LabOrderID", LabOrderID);
            DataTable table2 = new DataTable();
            SqlDataAdapter ad2 = new SqlDataAdapter(cmd);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                ad2.Fill(table2);
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                gridLabOrderDetails.DataSource = table2;
                gridLabOrderDetails.DataBind();
            }
        }

        protected void gridVisits_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            var ID = gridVisits.DataKeys[e.NewSelectedIndex].Value;
            string PatientID = "0";
            string HospitalID = "0";
            string DoctorID = "0";
            string PrescriptionID = "0";
            string LabOrderID = "0";
            string ICDID = "0";

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectVisit1";
            cmd.Parameters.AddWithValue("@ID", ID);
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                txtIDView.Text = reader["ID"].ToString();
                txtDateView.Text = reader["Date"].ToString();
                PatientID = reader["PatientID"].ToString();
                HospitalID = reader["HospitalID"].ToString();
                DoctorID = reader["DoctorID"].ToString();
                PrescriptionID = reader["PrescriptionID"].ToString();
                LabOrderID = reader["LabOrderID"].ToString();
                ICDID = reader["ICDID"].ToString();
                txtOutcomeView.Text = reader["Outcome"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectPatient1";
            cmd.Parameters.AddWithValue("@ID", PatientID);
            SqlDataReader reader2;
            try
            {
                con.Open();
                reader2 = cmd.ExecuteReader();
                reader2.Read();
                txtPatientView.Text = reader2["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectHospital1";
            cmd.Parameters.AddWithValue("@ID", HospitalID);
            SqlDataReader reader3;
            try
            {
                con.Open();
                reader3 = cmd.ExecuteReader();
                reader3.Read();
                txtHospitalView.Text = reader3["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectDoctor1";
            cmd.Parameters.AddWithValue("@ID", DoctorID);
            SqlDataReader reader4;
            try
            {
                con.Open();
                reader4 = cmd.ExecuteReader();
                reader4.Read();
                txtDoctorView.Text = reader4["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectICD1";
            cmd.Parameters.AddWithValue("@ID", ICDID);
            SqlDataReader reader5;
            try
            {
                con.Open();
                reader5 = cmd.ExecuteReader();
                reader5.Read();
                txtICDView.Text = reader5["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectPrescriptionDetail2";
            cmd.Parameters.AddWithValue("@PrescriptionID", PrescriptionID);
            DataTable table = new DataTable();
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                ad.Fill(table);
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                gridPrescriptionDetails.DataSource = table;
                gridPrescriptionDetails.DataBind();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectLabOrderDetail2";
            cmd.Parameters.AddWithValue("@LabOrderID", LabOrderID);
            DataTable table2 = new DataTable();
            SqlDataAdapter ad2 = new SqlDataAdapter(cmd);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                ad2.Fill(table2);
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                gridLabOrderDetails.DataSource = table2;
                gridLabOrderDetails.DataBind();
            }
        }

        protected void gridPrescriptionDetails_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            var ID = gridPrescriptionDetails.DataKeys[e.NewSelectedIndex].Value;
            string DrugID = "0";
            string PrescriptionID = "0";

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand("SELECT * FROM PrescriptionDetails WHERE ID='" + ID.ToString() + "'", con);
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                txtPrescriptionIDView.Text = reader["ID"].ToString();
                txtPrescriptionQuantityView.Text = reader["Quantity"].ToString();
                txtPrescriptionDoseView.Text = reader["Dose"].ToString();
                txtPrescriptionInstructionView.Text = reader["Instruction"].ToString();
                DrugID = reader["DrugID"].ToString();
                PrescriptionID = reader["PrescriptionID"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand("SELECT * FROM Drugs WHERE ID='" + DrugID + "'", con);
            SqlDataReader reader2;
            try
            {
                con.Open();
                reader2 = cmd.ExecuteReader();
                reader2.Read();
                txtPrescriptionDrugView.Text = reader2["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand("SELECT * FROM Prescriptions WHERE ID='" + PrescriptionID + "'", con);
            SqlDataReader reader3;
            try
            {
                con.Open();
                reader3 = cmd.ExecuteReader();
                reader3.Read();
                txtPrescriptionDateView.Text = reader3["Date"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('3rd.'" + ex.ToString() + ");</script");
            }
            finally
            {
                con.Close();
            }
        }

        protected void gridLabOrderDetails_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            var ID = gridLabOrderDetails.DataKeys[e.NewSelectedIndex].Value;
            string MedicalServiceID = "0";
            string LabOrderID = "0";

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand("SELECT * FROM LabOrderDetails WHERE ID='" + ID.ToString() + "'", con);
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                txtLabOrderIDView.Text = reader["ID"].ToString();
                txtLabOrderResultView.Text = reader["Result"].ToString();
                MedicalServiceID = reader["MedicalServiceID"].ToString();
                LabOrderID = reader["LabOrderID"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand("SELECT * FROM MedicalServices WHERE ID='" + MedicalServiceID + "'", con);
            SqlDataReader reader2;
            try
            {
                con.Open();
                reader2 = cmd.ExecuteReader();
                reader2.Read();
                txtLabOrderMedView.Text = reader2["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand("SELECT * FROM LabOrders WHERE ID='" + LabOrderID + "'", con);
            SqlDataReader reader3;
            try
            {
                con.Open();
                reader3 = cmd.ExecuteReader();
                reader3.Read();
                txtLabOrderDateView.Text = reader3["Date"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }
        }

        protected void btnSearch2_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSearchVisit";
            cmd.Parameters.AddWithValue("@Date", txtDateSearch.Text.Trim());
            cmd.Parameters.AddWithValue("@PatientID", dropPatientSearch.SelectedValue);
            cmd.Parameters.AddWithValue("@HospitalID", dropHospitalSearch.SelectedValue);
            cmd.Parameters.AddWithValue("@DoctorID", dropDoctorSearch.SelectedValue);
            cmd.Parameters.AddWithValue("@PrescriptionID", dropPrescriptionSearch.SelectedValue);
            cmd.Parameters.AddWithValue("@LabOrderID", dropLabOrderSearch.SelectedValue);
            cmd.Parameters.AddWithValue("@ICDID", dropICDSearch.SelectedValue);
            cmd.Parameters.AddWithValue("@Outcome", dropOutcomeSearch.SelectedValue);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                ad.Fill(table);
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            gridVisits2.DataSource = table;
            gridVisits2.DataBind();
            PopulateData();
        }
    }
}