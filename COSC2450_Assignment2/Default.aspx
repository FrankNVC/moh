﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="COSC2450_Assignment2.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="title">
        <h1>Welcome to the ERM SYSTEM</h1>
    </div>
    <div class="title">
        <h2>This is the HomePage.</h2>
    </div>
    <div>
        <p>
            <asp:HyperLink ID="HyperLink1" runat="server" Text="Member Page Only" NavigateUrl="~/Members/Doctors.aspx">Member</asp:HyperLink>
            <asp:HyperLink ID="HyperLink2" runat="server" Text="Admin Page Only" NavigateUrl="~/Admin/Update.aspx">Admin</asp:HyperLink>
        </p>
        <asp:Panel ID="Panel1" runat="server">
            <asp:LoginView ID="LoginView1" runat="server">
                <AnonymousTemplate>
                    You are not logged in. Click the Login link to sign in.<br />
                    <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Register.aspx">Register</asp:HyperLink>
                    &nbsp;
                </AnonymousTemplate>
                <LoggedInTemplate>
                    You are logged in. Welcome <asp:LoginName ID="LoginName2" runat="server" />
                </LoggedInTemplate>
            </asp:LoginView>
            <br />
        </asp:Panel>        
    </div>
</asp:Content>