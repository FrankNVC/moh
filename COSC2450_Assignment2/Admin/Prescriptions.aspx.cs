﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data.SqlClient;

namespace COSC2450_Assignment2
{
    public partial class Prescriptions : System.Web.UI.Page
    {
        private string conStr = WebConfigurationManager.ConnectionStrings["SqlService"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateData();
            }
        }

        protected void gridPrescriptions_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridPrescriptions.PageIndex = e.NewPageIndex;
            PopulateData();
        }

        protected void EditRow(object sender, GridViewEditEventArgs e)
        {
            gridPrescriptions.EditIndex = e.NewEditIndex;
            PopulateData();
        }

        protected void CancelEditRow(object sender, GridViewCancelEditEventArgs e)
        {
            gridPrescriptions.EditIndex = -1;
            PopulateData();
        }

        protected void DeleteRow(object sender, GridViewDeleteEventArgs e)
        {
            var ID = gridPrescriptions.DataKeys[e.RowIndex].Value;

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spDeletePrescription";
            cmd.Parameters.AddWithValue("@ID", ID);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                PopulateData();
            }
        }

        protected void UpdateRow(object sendedr, GridViewUpdateEventArgs e)
        {
            var ID = gridPrescriptions.DataKeys[e.RowIndex].Value;

            GridViewRow row = gridPrescriptions.Rows[e.RowIndex] as GridViewRow;
            TextBox tDate = row.FindControl("txtDate") as TextBox;
            DropDownList dropDoctor = row.FindControl("dropDoctor") as DropDownList;

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spUpdatePrescription";

            cmd.Parameters.AddWithValue("@ID", ID);
            cmd.Parameters.AddWithValue("@Date", tDate.Text.Trim());
            cmd.Parameters.AddWithValue("@DID", dropDoctor.SelectedValue);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                gridPrescriptions.EditIndex = -1;
                PopulateData();
            }
        }

        private void PopulateData()
        {
            dropDoctor2.Items.Clear();
            dropPrescription.Items.Clear();
            dropPrescriptionID.Items.Clear();
            dropDoctorSearch.Items.Clear();

            dropDoctorSearch.Items.Add("0");

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectPrescriptions";
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                ad.Fill(table);
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            gridPrescriptions.DataSource = table;
            gridPrescriptions.DataBind();

            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = reader["ID"] + ", " + reader["Date"];
                    newItem.Value = reader["ID"].ToString();
                    dropPrescription.Items.Add(newItem);
                    dropPrescriptionID.Items.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectDrugs";
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = reader["ID"] + ", " + reader["Name"];
                    newItem.Value = reader["ID"].ToString();
                    dropDrug.Items.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectDoctors";
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = reader["ID"] + ", " + reader["Name"];
                    newItem.Value = reader["ID"].ToString();
                    dropDoctor2.Items.Add(newItem);
                    dropDoctorSearch.Items.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spInsertPrescription";
            cmd.Parameters.AddWithValue("@Date", txtDate2.Text.Trim());
            cmd.Parameters.AddWithValue("@DID", dropDoctor2.SelectedValue);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                Response.Write("<script language='javascript'>alert('1 record added.');</script");
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                PopulateData();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string DID = "0";
            string HID = "0";

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectPrescription1";
            cmd.Parameters.AddWithValue("@ID", dropPrescription.SelectedValue);
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                txtIDView.Text = reader["ID"].ToString();
                txtDateView.Text = reader["Date"].ToString();
                DID = reader["DID"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectDoctor1";
            cmd.Parameters.AddWithValue("@ID", DID);
            SqlDataReader reader2;
            try
            {
                con.Open();
                reader2 = cmd.ExecuteReader();
                reader2.Read();
                txtDoctorView.Text = reader2["Name"].ToString();
                HID = reader2["HID"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }
        }

        protected void gridPrescriptions_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            var ID = gridPrescriptions.DataKeys[e.NewSelectedIndex].Value;
            string DID = "0";
            string HID = "0";

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectPrescription1";
            cmd.Parameters.AddWithValue("@ID", ID);
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                txtIDView.Text = reader["ID"].ToString();
                txtDateView.Text = reader["Date"].ToString();
                DID = reader["DID"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectDoctor1";
            cmd.Parameters.AddWithValue("@ID", DID);
            SqlDataReader reader2;
            try
            {
                con.Open();
                reader2 = cmd.ExecuteReader();
                reader2.Read();
                txtDoctorView.Text = reader2["Name"].ToString();
                HID = reader2["HID"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spInsertPrescriptionDetail";
            cmd.Parameters.AddWithValue("@DrugID", dropDrug.SelectedValue);
            cmd.Parameters.AddWithValue("@PrescriptionID", dropPrescriptionID.SelectedValue);
            cmd.Parameters.AddWithValue("@Quantity", txtQuantity.Text);
            cmd.Parameters.AddWithValue("@Dose", txtDose.Text);
            cmd.Parameters.AddWithValue("@Instruction", txtInstruction.Text);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                Response.Write("<script language='javascript'>alert('1 record added.');</script");
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                PopulateData();
            }
        }

        protected void btnSearch2_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSearchPrescription";
            cmd.Parameters.AddWithValue("@Date", txtDateSearch.Text.Trim());
            cmd.Parameters.AddWithValue("@DID", dropDoctorSearch.SelectedValue);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                ad.Fill(table);
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            gridPrescriptions2.DataSource = table;
            gridPrescriptions2.DataBind();
            PopulateData();
        }
    }
}