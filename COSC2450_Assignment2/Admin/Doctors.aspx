﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Doctors.aspx.cs" Inherits="COSC2450_Assignment2.Doctors" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="title">
        <h2>This is the Doctor Page.</h2>
    </div>
    <cc1:Accordion ID="Accordion1" runat="server" HeaderCssClass="accHeader" HeaderSelectedCssClass="accHeader" ContentCssClass="accContent">
        <Panes>
            <cc1:AccordionPane ID="AccordionPane1" runat="server">
                <Header> View All Doctors</Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gridDoctors" runat="server" CellSpacing="2" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="ID"
                                ForeColor="#333333" GridLines="None" Width="600px" OnPageIndexChanging="gridDoctors_PageIndexChanging"
                                OnRowEditing="EditRow" OnRowCancelingEdit="CancelEditRow" OnRowUpdating="UpdateRow" OnRowDeleting="DeleteRow" OnSelectedIndexChanging="gridDoctors_SelectedIndexChanging">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <Columns>
                                    <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID" />
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <%# Eval("Name") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtName" runat="server" Text='<%# Eval("Name") %>' />
                                            <asp:RequiredFieldValidator ID="ReqFieldValName" runat="server" ControlToValidate="txtName" ValidationGroup="update" ErrorMessage="Input Name" />
                                            <div>
                                                <asp:RegularExpressionValidator ID="RegExValName" runat="server" ControlToValidate="txtName" ValidationGroup="update" ValidationExpression="^[a-zA-Z\.\'\-_\s]{1,40}$" ErrorMessage="Inputted Name Not Valid"></asp:RegularExpressionValidator>
                                            </div>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Gender">
                                        <ItemTemplate>
                                            <%# Eval("Gender") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtGender" runat="server" Text='<%# Eval("Gender") %>' size="5" />
                                            <asp:RequiredFieldValidator ID="ReqFieldValGender" runat="server" ControlToValidate="txtGender" ValidationGroup="update" ErrorMessage="Input Gender" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date of Birth">
                                        <ItemTemplate>
                                            <%# Eval("DOB", "{0:MM/dd/yyyy}") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtDOB" runat="server" Text='<%# Eval("DOB", "{0:MM/dd/yyyy}") %>' size="15" /><a runat="server" id="hrefDOB"> Calendar</a>
                                            <cc1:CalendarExtender ID="CalendarExt" runat="server" Format="MM/dd/yyyy" TargetControlID="txtDOB" PopupButtonID="hrefDOB" Enabled="true"></cc1:CalendarExtender>
                                            <div>
                                                <cc1:MaskedEditExtender ID="MEEDate" runat="server" TargetControlID="txtDOB" Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true"></cc1:MaskedEditExtender>
                                                <cc1:MaskedEditValidator ID="MEVDate" runat="server" ControlToValidate="txtDOB" ValidationGroup="update" ControlExtender="MEEDate" IsValidEmpty="false" EmptyValueMessage="Input Date" InvalidValueMessage="Inputted Date not Valid"></cc1:MaskedEditValidator>
                                            </div>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="LicenseNo">
                                        <ItemTemplate>
                                            <%# Eval("LicenseNo") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtLicenseNo" runat="server" Text='<%# Eval("LicenseNo") %>' size="5" />
                                            <asp:RequiredFieldValidator ID="ReqFieldValLicense" runat="server" ControlToValidate="txtLicenseNo" ValidationGroup="update" ErrorMessage="Input License No" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Address">
                                        <ItemTemplate>
                                            <%# Eval("Address") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtAddress" runat="server" Text='<%# Eval("Address") %>' />
                                            <asp:RequiredFieldValidator ID="ReqFieldValAddress" runat="server" ControlToValidate="txtAddress" ValidationGroup="update" ErrorMessage="Input Address" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="Edit" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="lnkUpdate" runat="server" Text="Update" CommandName="Update" CausesValidation="true" ValidationGroup="update" />
                                            <asp:LinkButton ID="lnkCancel" runat="server" Text="Cancel" CommandName="Cancel" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <span onclick="return confirm('Are you sure to delete?')">
                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" ForeColor="Red" CommandName="Delete" />
                                            </span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <PagerStyle BackColor="#284755" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="true" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284755" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                            <asp:Panel ID="pnlDoctor" runat="server">
                                <div>
                                    <h2>Doctor</h2>
                                </div>
                                <div class="form">
                                    <fieldset>
                                        <dl>
                                            <dt>
                                                <label>ID:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtIDView" runat="server" size="5"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Name:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtNameView" runat="server"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Gender:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtGenderView" runat="server" size="5"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Date of Birth:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtDateView" runat="server" size="10"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>License No:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtLicenseView" runat="server" size="10"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Address:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtAddressView" runat="server" size="50"></asp:TextBox>
                                            </dd>
                                        </dl>                                        
                                        <dl>
                                            <dt>
                                                <asp:DropDownList ID="dropDoctor" runat="server" AutoPostBack="True"></asp:DropDownList>
                                                <cc1:ListSearchExtender ID="dropDoctor_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropDoctor" PromptText="Type to Search" IsSorted="true" PromptPosition="Bottom" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                            </dt>                                            
                                        </dl>
                                        <dl>
                                            <dt></dt>
                                            <dd>
                                                <asp:Button ID="btnSearch" runat="server" Text="Search One" OnClick="btnSearch_Click" />
                                            </dd>
                                        </dl>
                                    </fieldset>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlSearch" runat="server">
                                <asp:TextBox ID="txtNameSearch" runat="server" Text="Name"></asp:TextBox>
                                <asp:DropDownList ID="dropGenderSearch" runat="server"></asp:DropDownList>
                                <asp:TextBox ID="txtDOBSearch" runat="server" Text="Birthday"></asp:TextBox>
                                <asp:TextBox ID="txtLicenseNoSearch" runat="server" Text="LicenseNo"></asp:TextBox>
                                <asp:TextBox ID="txtAddressSearch" runat="server" Text="Address"></asp:TextBox>
                                <asp:Button ID="btnSearch2" runat="server" Text="Search Many" OnClick="btnSearch2_Click" />
                            </asp:Panel>
                            <asp:GridView ID="gridDoctors2" runat="server" CellSpacing="2" DataKeyNames="ID"
                                ForeColor="#333333" GridLines="None" Width="600px">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <PagerStyle BackColor="#284755" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="true" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284755" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </cc1:AccordionPane>
            <cc1:AccordionPane ID="AccordionPane2" runat="server">
                <Header>Insert new Doctor</Header>
                <Content>
                    <asp:Panel ID="Panel1" runat="server">
                        <div>
                            <h2>Insert Doctor Form</h2>
                        </div>
                        <div class="form">
                            <fieldset>
                                <dl>
                                    <dt>
                                        <label>Doctor Name:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtName2" runat="server" size="30"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ReqFieldValName2" runat="server" ControlToValidate="txtName2" ValidationGroup="insert" ErrorMessage="Input Name"></asp:RequiredFieldValidator>
                                        <div>
                                            <asp:RegularExpressionValidator ID="RegExValName2" runat="server" ControlToValidate="txtName2" ValidationGroup="insert" ValidationExpression="^(\w+)(\s\w+)*$" ErrorMessage="Inputted Name not Valid"></asp:RegularExpressionValidator>
                                        </div>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Gender:</label></dt>
                                    <dd>
                                        <asp:DropDownList ID="dropGender2" runat="server"></asp:DropDownList>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Birthday:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtDOB2" runat="server" size="20"></asp:TextBox>(mm/dd/yyyy)<a runat="server" id="hrefDOB2"> Calendar</a>
                                        <cc1:CalendarExtender ID="CalendarExt2" runat="server" Format="MM/dd/yyyy" TargetControlID="txtDOB2" PopupButtonID="hrefDOB2" Enabled="true"></cc1:CalendarExtender>
                                        <div>
                                            <cc1:MaskedEditExtender ID="MEEBirthday" runat="server" TargetControlID="txtDOB2" Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true"></cc1:MaskedEditExtender>
                                            <cc1:MaskedEditValidator ID="MEVBirthday" runat="server" ControlToValidate="txtDOB2" ValidationGroup="insert" ControlExtender="MEEBirthday" IsValidEmpty="false" EmptyValueMessage="Input Date" InvalidValueMessage="Inputted Date not Valid"></cc1:MaskedEditValidator>
                                        </div>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>License No:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtLicenseNo2" runat="server" size="20"></asp:TextBox>(Dxxxxx)
                                        <asp:RequiredFieldValidator ID="ReqFieldValLicense2" runat="server" ControlToValidate="txtLicenseNo2" ValidationGroup="insert" ErrorMessage="Input License"></asp:RequiredFieldValidator>
                                        <div>
                                            <asp:RegularExpressionValidator ID="RegExValLicense2" runat="server" ControlToValidate="txtLicenseNo2" ValidationGroup="insert" ValidationExpression="^D[0-9]{5}$" ErrorMessage="Inputted License not Valid"></asp:RegularExpressionValidator>
                                        </div>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Address:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtAddress2" runat="server" size="30"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ReqFieldValAddress2" runat="server" ControlToValidate="txtAddress2" ValidationGroup="insert" ErrorMessage="Input Address"></asp:RequiredFieldValidator>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt></dt>
                                    <dd>
                                        <asp:Button ID="btnSave" CssClass="bt_green" runat="server" Text="Add new Doctor" BackColor="Lime" CausesValidation="true" ValidationGroup="insert" OnClick="btnSave_Click" />
                                    </dd>
                                </dl>
                            </fieldset>
                        </div>
                    </asp:Panel>
                </Content>
            </cc1:AccordionPane>
        </Panes>
    </cc1:Accordion>
</asp:Content>
