﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Visits.aspx.cs" Inherits="COSC2450_Assignment2.Visits" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="title">
        <h2>This is the Visit Page.</h2>
    </div>
    <cc1:Accordion ID="Accordion1" runat="server" HeaderCssClass="accHeader" HeaderSelectedCssClass="accHeader" ContentCssClass="accContent">
        <Panes>
            <cc1:AccordionPane ID="AccordionPane1" runat="server">
                <Header> View All Visits</Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gridVisits" runat="server" CellSpacing="2" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" DataKeyNames="ID"
                                ForeColor="#333333" GridLines="None" DataKeyName="ID" Width="600px" OnPageIndexChanging="gridVisits_PageIndexChanging"
                                OnRowEditing="EditRow" OnRowCancelingEdit="CancelEditRow" OnRowUpdating="UpdateRow" OnRowDeleting="DeleteRow" OnSelectedIndexChanging="gridVisits_SelectedIndexChanging">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <Columns>
                                    <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID" />
                                    <asp:TemplateField HeaderText="Visit Date">
                                        <ItemTemplate>
                                            <%# Eval("Date", "{0:MM/dd/yyyy}") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtDate" runat="server" Text='<%# Eval("Date", "{0:MM/dd/yyyy}") %>' size="15" ></asp:TextBox><a runat="server" id="hrefDate2">Calendar</a>
                                            <cc1:CalendarExtender ID="CalendarExt2" runat="server" Format="MM/dd/yyyy" TargetControlID="txtDate" PopupButtonID="hrefDate2" Enabled="true"></cc1:CalendarExtender>
                                            <cc1:MaskedEditExtender ID="MEEDate" runat="server" TargetControlID="txtDate" Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true"></cc1:MaskedEditExtender>
                                            <cc1:MaskedEditValidator ID="MEVDate" runat="server" ControlToValidate="txtDate" ValidationGroup="insert" ControlExtender="MEEDate" IsValidEmpty="false" EmptyValueMessage="Input Date" InvalidValueMessage="Inputted Date not Valid"></cc1:MaskedEditValidator>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Patient">
                                        <ItemTemplate>
                                            <%# Eval("PatientID") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="dropPatient" runat="server" AutoPostBack="True" DataSourceID="sourcePatients" DataTextField="ID" DataValueField="ID"></asp:DropDownList>
                                            <cc1:ListSearchExtender ID="dropPatient_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropPatient" IsSorted="true" QueryPattern="Contains">
                                            </cc1:ListSearchExtender>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Hospital">
                                        <ItemTemplate>
                                            <%# Eval("HospitalID") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="dropHospital" runat="server" AutoPostBack="True" DataSourceID="sourceHospitals" DataTextField="ID" DataValueField="ID"></asp:DropDownList>
                                            <cc1:ListSearchExtender ID="dropHospital_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropHospital" IsSorted="true" QueryPattern="Contains">
                                            </cc1:ListSearchExtender>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Doctor">
                                        <ItemTemplate>
                                            <%# Eval("DoctorID") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="dropDoctor" runat="server" AutoPostBack="True" DataSourceID="sourceDoctors" DataTextField="ID" DataValueField="ID"></asp:DropDownList>
                                            <cc1:ListSearchExtender ID="dropDoctor_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropDoctor" IsSorted="true" QueryPattern="Contains">
                                            </cc1:ListSearchExtender>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Prescription">
                                        <ItemTemplate>
                                            <%# Eval("PrescriptionID") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="dropPrescription" runat="server" AutoPostBack="True" DataSourceID="sourcePrescriptions" DataTextField="Date" DataTextFormatString="{0:dd/MM/yyyy}" DataValueField="ID"></asp:DropDownList>
                                            <cc1:ListSearchExtender ID="dropPrescription_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropPrescription" IsSorted="true" QueryPattern="Contains">
                                            </cc1:ListSearchExtender>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lab Order">
                                        <ItemTemplate>
                                            <%# Eval("LabOrderID") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="dropLabOrder" runat="server" AutoPostBack="True" DataSourceID="sourceLabOrders" DataTextField="Date" DataTextFormatString="{0:dd/MM/yyyy}" DataValueField="ID"></asp:DropDownList>
                                            <cc1:ListSearchExtender ID="dropLabOrder_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropLabOrder" IsSorted="true" QueryPattern="Contains">
                                            </cc1:ListSearchExtender>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ICD Diagnosis">
                                        <ItemTemplate>
                                            <%# Eval("ICDID") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="dropICD" runat="server" AutoPostBack="True" DataSourceID="sourceICDs" DataTextField="ID" DataValueField="ID"></asp:DropDownList>
                                            <cc1:ListSearchExtender ID="dropICD_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropICD" IsSorted="true" QueryPattern="Contains">
                                            </cc1:ListSearchExtender>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Outcome">
                                        <ItemTemplate>
                                            <%# Eval("Outcome") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtOutcome" runat="server" size="20" Text='<%# Eval("Outcome") %>' ></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqFieldValOutcome" runat="server" ControlToValidate="txtOutcome" ValidationGroup="update" ErrorMessage="Input Outcome" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="Edit" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="lnkUpdate" runat="server" Text="Update" CommandName="Update" CausesValidation="true" ValidationGroup="update" />
                                            <asp:LinkButton ID="lnkCancel" runat="server" Text="Cancel" CommandName="Cancel" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <span onclick="return confirm('Are you sure to delete?')">
                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" ForeColor="Red" CommandName="Delete" />
                                            </span>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                    
                                </Columns>
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <PagerStyle BackColor="#284755" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="true" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284755" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                            <asp:Panel ID="pnlVisit" runat="server">
                                <div>
                                    <h2>Visit</h2>
                                </div>
                                <div class="form">
                                    <fieldset>
                                        <dl>
                                            <dt>
                                                <label>ID:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtIDView" runat="server" size="5"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Date:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtDateView" runat="server" size="20"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Patient Name:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtPatientView" runat="server" size="30"></asp:TextBox>
                                            </dd>
                                        </dl>
                                         <dl>
                                            <dt>
                                                <label>Hospital Name:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtHospitalView" runat="server" size="30"></asp:TextBox>
                                            </dd>
                                        </dl>
                                         <dl>
                                            <dt>
                                                <label>Doctor Name:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtDoctorView" runat="server" size="30"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>ICD Diagnosis:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtICDView" runat="server" size="50"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Outcome:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtOutcomeView" runat="server" size="10"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gridPrescriptionDetails" runat="server" CellSpacing="2" AutoGenerateColumns="False" AllowSorting="True" DataKeyNames="ID"
                                                        ForeColor="#333333" GridLines="None" DataKeyName="ID" Width="600px" OnSelectedIndexChanging="gridPrescriptionDetails_SelectedIndexChanging">
                                                        <Columns>
                                                            <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID" />
                                                            <asp:TemplateField HeaderText="Drug">
                                                                <ItemTemplate>
                                                                    <%# Eval("DrugID") %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Prescription ID">
                                                                <ItemTemplate>
                                                                    <%# Eval("PrescriptionID") %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Quantity">
                                                                <ItemTemplate>
                                                                    <%# Eval("Quantity") %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Dose">
                                                                <ItemTemplate>
                                                                    <%# Eval("Dose") %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Instruction">
                                                                <ItemTemplate>
                                                                    <%# Eval("Instruction") %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                                                        </Columns>
                                                        <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                                        <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                                        <PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
                                                        <RowStyle BackColor="#EFF3FB"></RowStyle>
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB"></SortedAscendingCellStyle>
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1"></SortedAscendingHeaderStyle>
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF"></SortedDescendingCellStyle>
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE"></SortedDescendingHeaderStyle>
                                                    </asp:GridView>
                                                    <asp:Panel ID="pnlPrescriptionDetail" runat="server">
                                                        <div>
                                                            <h2>Prescription Detail</h2>
                                                        </div>
                                                        <div class="form">
                                                            <fieldset>
                                                                <dl>
                                                                    <dt>
                                                                        <label>ID:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtPrescriptionIDView" runat="server" size="5"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                                <dl>
                                                                    <dt>
                                                                        <label>Date:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtPrescriptionDateView" runat="server" size="20"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                                <dl>
                                                                    <dt>
                                                                        <label>Drug:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtPrescriptionDrugView" runat="server" size="50"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                                <dl>
                                                                    <dt>
                                                                        <label>Quantity:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtPrescriptionQuantityView" runat="server" size="5"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                                <dl>
                                                                    <dt>
                                                                        <label>Dose:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtPrescriptionDoseView" runat="server" size="5"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                                <dl>
                                                                    <dt>
                                                                        <label>Instruction:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtPrescriptionInstructionView" runat="server"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                            </fieldset>
                                                        </div>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </dl>
                                        <dl>
                                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gridLabOrderDetails" runat="server" CellSpacing="2" AutoGenerateColumns="False" AllowSorting="True" DataKeyNames="ID"
                                                        ForeColor="#333333" GridLines="None" DataKeyName="ID" Width="600px" OnSelectedIndexChanging="gridLabOrderDetails_SelectedIndexChanging">
                                                        <Columns>
                                                            <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID" />
                                                            <asp:TemplateField HeaderText="Medical Service">
                                                                <ItemTemplate>
                                                                    <%# Eval("MedicalServiceID") %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Result">
                                                                <ItemTemplate>
                                                                    <%# Eval("Result") %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Lab Order ID">
                                                                <ItemTemplate>
                                                                    <%# Eval("LabOrderID") %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                                                        </Columns>
                                                        <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                                                        <EditRowStyle BackColor="#2461BF"></EditRowStyle>
                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></FooterStyle>
                                                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                                        <PagerStyle HorizontalAlign="Center" BackColor="#2461BF" ForeColor="White"></PagerStyle>
                                                        <RowStyle BackColor="#EFF3FB"></RowStyle>
                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333"></SelectedRowStyle>
                                                        <SortedAscendingCellStyle BackColor="#F5F7FB"></SortedAscendingCellStyle>
                                                        <SortedAscendingHeaderStyle BackColor="#6D95E1"></SortedAscendingHeaderStyle>
                                                        <SortedDescendingCellStyle BackColor="#E9EBEF"></SortedDescendingCellStyle>
                                                        <SortedDescendingHeaderStyle BackColor="#4870BE"></SortedDescendingHeaderStyle>
                                                    </asp:GridView>
                                                    <asp:Panel ID="pnlLabOrder" runat="server">
                                                        <div>
                                                            <h2>Lab Order Detail</h2>
                                                        </div>
                                                        <div class="form">
                                                            <fieldset>
                                                                <dl>
                                                                    <dt>
                                                                        <label>ID:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtLabOrderIDView" runat="server" size="5"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                                <dl>
                                                                    <dt>
                                                                        <label>Date:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtLabOrderDateView" runat="server"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                                <dl>
                                                                    <dt>
                                                                        <label>Medical Service:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtLabOrderMedView" runat="server" size="50"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                                <dl>
                                                                    <dt>
                                                                        <label>Result:</label></dt>
                                                                    <dd>
                                                                        <asp:TextBox ID="txtLabOrderResultView" runat="server" size="10"></asp:TextBox>
                                                                    </dd>
                                                                </dl>
                                                            </fieldset>
                                                        </div>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <asp:DropDownList ID="dropVisit" runat="server" AutoPostBack="True"></asp:DropDownList>
                                                <cc1:ListSearchExtender ID="dropVisit_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropVisit" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                            </dt>                                            
                                        </dl>
                                        <dl>
                                            <dt></dt>
                                            <dd>
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                                            </dd>
                                        </dl>
                                    </fieldset>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlSearch" runat="server">
                                <asp:TextBox ID="txtDateSearch" runat="server" Text="Date"></asp:TextBox>
                                <asp:DropDownList ID="dropPatientSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropPatientSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropPatientSearch" IsSorted="true" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:DropDownList ID="dropHospitalSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropHospitalSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropHospitalSearch" IsSorted="true" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:DropDownList ID="dropDoctorSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropDoctorSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropDoctorSearch" IsSorted="true" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:DropDownList ID="dropPrescriptionSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropPrescriptionSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropPrescriptionSearch" IsSorted="true" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:DropDownList ID="dropLabOrderSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropLabOrder_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropLabOrderSearch" IsSorted="true" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:DropDownList ID="dropICDSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropICDSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropICDSearch" IsSorted="true" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:DropDownList ID="dropOutcomeSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropOutcomeSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropOutcomeSearch" IsSorted="true" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:Button ID="btnSearch2" runat="server" Text="Search Many" OnClick="btnSearch2_Click" />
                            </asp:Panel>
                            <asp:GridView ID="gridVisits2" runat="server" CellSpacing="2" DataKeyNames="ID"
                                ForeColor="#333333" GridLines="None" Width="600px">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <PagerStyle BackColor="#284755" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="true" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284755" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </cc1:AccordionPane>
            <cc1:AccordionPane ID="AccordionPane2" runat="server">
                <Header>Insert new Visit</Header>
                <Content>
                    <asp:Panel ID="Panel1" runat="server">
                        <div>
                            <h2>Insert Visit Form</h2>
                        </div>
                        <div class="form">
                            <fieldset>
                                <dl>
                                    <dt>
                                        <label>Date:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtDate2" runat="server" size="20"></asp:TextBox>(MM/dd/yyyy)<a runat="server" id="hrefDate">Calendar</a>
                                        <cc1:CalendarExtender ID="CalendarExt" runat="server" Format="MM/dd/yyyy" TargetControlID="txtDate2" PopupButtonID="hrefDate" Enabled="true"></cc1:CalendarExtender>
                                        <div>
                                            <cc1:MaskedEditExtender ID="MEEDate" runat="server" TargetControlID="txtDate2" Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true"></cc1:MaskedEditExtender>
                                            <cc1:MaskedEditValidator ID="MEVDate" runat="server" ControlToValidate="txtDate2" ValidationGroup="insert" ControlExtender="MEEDate" IsValidEmpty="false" EmptyValueMessage="Input Date" InvalidValueMessage="Inputted Date not Valid"></cc1:MaskedEditValidator>
                                        </div>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Patient:</label></dt>
                                    <dd>
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="dropPatient2" runat="server" AutoPostBack="True"></asp:DropDownList><a runat="server" id="hrefPatient" href="Patients.aspx">Insert Patient</a>
                                                <cc1:ListSearchExtender ID="dropPatient2_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropPatient2" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Hospital:</label></dt>
                                    <dd>
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="dropHospital2" runat="server" AutoPostBack="True"></asp:DropDownList><a runat="server" id="hrefHospital" href="Hospitals.aspx">Insert Hospital</a>
                                                <cc1:ListSearchExtender ID="dropHospital2_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropHospital2" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Doctor:</label></dt>
                                    <dd>
                                        <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="dropDoctor4" runat="server" AutoPostBack="True"></asp:DropDownList><a runat="server" id="hrefDoctor" href="Doctors.aspx">Insert Doctor</a>
                                                <cc1:ListSearchExtender ID="dropDoctor4_ListSearchExtender1" runat="server" Enabled="true" TargetControlID="dropDoctor4" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Prescription:</label></dt>
                                    <dd>
                                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="dropPrescriptionID2" runat="server" AutoPostBack="True"></asp:DropDownList>
                                                <cc1:ListSearchExtender ID="dropPrescriptionID2_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropPrescriptionID2" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Lab Order:</label></dt>
                                    <dd>
                                        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="dropLabOrderID2" runat="server" AutoPostBack="True"></asp:DropDownList>
                                                <cc1:ListSearchExtender ID="dropLabOrderID2_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropLabOrderID2" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Diagnosis:</label></dt>
                                    <dd>
                                        <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="dropICD2" runat="server" AutoPostBack="True"></asp:DropDownList>
                                                <cc1:ListSearchExtender ID="dropICD2_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropICD2" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </dd>
                                    <dt></dt>
                                    <dd>
                                        <a runat="server" id="hrefICD" href="ICDs.aspx">Insert ICD</a>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Outcome:</label></dt>
                                    <dd>
                                        <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="dropOutcome" runat="server" AutoPostBack="True"></asp:DropDownList>
                                            </ContentTemplate>                                            
                                        </asp:UpdatePanel>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt></dt>
                                    <dd>
                                        <asp:Button ID="btnSave" CssClass="bt_green" runat="server" Text="Add new Visit" BackColor="Lime" CausesValidation="true" ValidationGroup="insert" OnClick="btnSave_Click" />
                                    </dd>
                                </dl>
                                <dl>
                                    <h2>Insert Prescription Form</h2>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Date:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtDate3" runat="server" size="20"></asp:TextBox>(MM/dd/yyyy)<a runat="server" id="A1">Calendar</a>
                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="MM/dd/yyyy" TargetControlID="txtDate3" PopupButtonID="A1" Enabled="true"></cc1:CalendarExtender>
                                        <div>
                                            <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="txtDate3" Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true"></cc1:MaskedEditExtender>
                                            <cc1:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlToValidate="txtDate3" ValidationGroup="insertPrescription" ControlExtender="MaskedEditExtender1" IsValidEmpty="false" EmptyValueMessage="Input Date" InvalidValueMessage="Inputted Date not Valid"></cc1:MaskedEditValidator>
                                        </div>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Doctor:</label></dt>
                                    <dd>
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="dropDoctor2" runat="server" AutoPostBack="True"></asp:DropDownList><a runat="server" id="hrefDoctor2" href="Doctors.aspx">Insert Doctor</a>
                                                <cc1:ListSearchExtender ID="dropDoctor_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropDoctor2" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt></dt>
                                    <dd>
                                        <asp:Button ID="btnOK1" CssClass="bt_green" runat="server" Text="Add new Prescription" BackColor="Lime" CausesValidation="true" ValidationGroup="insertPrescription" OnClick="btnOK1_Click"/>
                                    </dd>
                                </dl>
                                <dl>
                                    <h2>Insert Lab Order Form</h2>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Date:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtDate4" runat="server" size="20"></asp:TextBox>(MM/dd/yyyy)<a runat="server" id="A2">Calendar</a>
                                        <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Format="MM/dd/yyyy" TargetControlID="txtDate4" PopupButtonID="A2" Enabled="true"></cc1:CalendarExtender>
                                        <div>
                                            <cc1:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="txtDate4" Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true"></cc1:MaskedEditExtender>
                                            <cc1:MaskedEditValidator ID="MaskedEditValidator2" runat="server" ControlToValidate="txtDate4" ValidationGroup="insertLabOrder" ControlExtender="MaskedEditExtender2" IsValidEmpty="false" EmptyValueMessage="Input Date" InvalidValueMessage="Inputted Date not Valid"></cc1:MaskedEditValidator>
                                        </div>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Doctor:</label></dt>
                                    <dd>
                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="dropDoctor3" runat="server" AutoPostBack="True"></asp:DropDownList><a runat="server" id="hrefDoctor3" href="Doctors.aspx">Insert Doctor</a>
                                                <cc1:ListSearchExtender ID="dropDoctor3_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropDoctor3" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt></dt>
                                    <dd>
                                        <asp:Button ID="btnOK2" CssClass="bt_green" runat="server" Text="Add new LabOrder" BackColor="Lime" CausesValidation="true" ValidationGroup="insertLabOrder" OnClick="btnOK2_Click"/>
                                    </dd>
                                </dl>
                            </fieldset>
                        </div>
                    </asp:Panel>
                </Content>
            </cc1:AccordionPane>
        </Panes>
    </cc1:Accordion>
    <asp:SqlDataSource ID="sourcePatients" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM Patients"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sourceHospitals" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM Hospitals"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sourceDoctors" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM Doctors"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sourcePrescriptions" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM Prescriptions"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sourceLabOrders" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM LabOrders"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sourceICDs" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM ICDs"></asp:SqlDataSource>
</asp:Content>
