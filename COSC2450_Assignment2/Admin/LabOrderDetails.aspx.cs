﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data.SqlClient;

namespace COSC2450_Assignment2
{
    public partial class LabOrderDetails : System.Web.UI.Page
    {
        private string conStr = WebConfigurationManager.ConnectionStrings["SqlService"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateData();                
            }
        }

        protected void gridLabOrderDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridLabOrderDetails.PageIndex = e.NewPageIndex;
            PopulateData();
        }

        protected void EditRow(object sender, GridViewEditEventArgs e)
        {
            gridLabOrderDetails.EditIndex = e.NewEditIndex;
            PopulateData();
        }

        protected void CancelEditRow(object sender, GridViewCancelEditEventArgs e)
        {
            gridLabOrderDetails.EditIndex = -1;
            PopulateData();
        }

        protected void DeleteRow(object sender, GridViewDeleteEventArgs e)
        {
            var ID = gridLabOrderDetails.DataKeys[e.RowIndex].Value;

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spDeleteLabOrderDetail";
            cmd.Parameters.AddWithValue("@ID", ID);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem.'" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                PopulateData();
            }
        }

        protected void UpdateRow(object sendedr, GridViewUpdateEventArgs e)
        {
            var ID = gridLabOrderDetails.DataKeys[e.RowIndex].Value;

            GridViewRow row = gridLabOrderDetails.Rows[e.RowIndex] as GridViewRow;
            TextBox tResult = row.FindControl("txtResult") as TextBox;
            DropDownList dropMedicalService = row.FindControl("dropMedicalService") as DropDownList;
            DropDownList dropLabOrderID = row.FindControl("dropLabOrderID") as DropDownList;

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();            
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spUpdateLabOrderDetail";

            cmd.Parameters.AddWithValue("@ID", ID);
            cmd.Parameters.AddWithValue("@MedicalServiceID", dropMedicalService.SelectedValue);
            cmd.Parameters.AddWithValue("@Result", tResult.Text.Trim());
            cmd.Parameters.AddWithValue("@LabOrderID", dropLabOrderID.SelectedValue);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                gridLabOrderDetails.EditIndex = -1;
                PopulateData();
            }
        }

        private void PopulateData()
        {
            dropLabOrderDetail.Items.Clear();
            dropLabOrderID2.Items.Clear();
            dropMedicalService2.Items.Clear();
            dropLabOrderIDSearch.Items.Clear();
            dropMedicalServiceSearch.Items.Clear();

            dropLabOrderIDSearch.Items.Add("0");
            dropMedicalServiceSearch.Items.Add("0");

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectLabOrderDetails";
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                ad.Fill(table);
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem.'" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                gridLabOrderDetails.DataSource = table;
                gridLabOrderDetails.DataBind();
            }

            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = reader["ID"] + ", " + reader["MedicalServiceID"] + ", " + reader["LabOrderID"];
                    newItem.Value = reader["ID"].ToString();
                    dropLabOrderDetail.Items.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem.'" + ex.ToString() + "');</script");
            }
            finally 
            {
                con.Close(); 
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectMedicalServices";            
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = reader["ID"] + ", " + reader["Name"];
                    newItem.Value = reader["ID"].ToString();
                    dropMedicalService2.Items.Add(newItem);
                    dropMedicalServiceSearch.Items.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem.'" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectLabOrders";            
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = reader["ID"] + ", " + reader["Date"];
                    newItem.Value = reader["ID"].ToString();
                    dropLabOrderID2.Items.Add(newItem);
                    dropLabOrderIDSearch.Items.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem.'" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spInsertLabOrderDetail";
            cmd.Parameters.AddWithValue("@MedicalServiceID", dropMedicalService2.SelectedValue);
            cmd.Parameters.AddWithValue("@Result", txtResult2.Text.Trim());
            cmd.Parameters.AddWithValue("@LabOrderID", dropLabOrderID2.SelectedValue);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                Response.Write("<script language='javascript'>alert('1 record added.');</script");
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem.'" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                PopulateData();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string MedicalServiceID = "0";
            string LabOrderID = "0";
            string DID = "0";

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectLabOrderDetail1";
            cmd.Parameters.AddWithValue("@ID", dropLabOrderDetail.SelectedValue);
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                txtIDView.Text = reader["ID"].ToString();
                txtResultView.Text = reader["Result"].ToString();
                MedicalServiceID = reader["MedicalServiceID"].ToString();
                LabOrderID = reader["LabOrderID"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem.'" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectMedicalService1";
            cmd.Parameters.AddWithValue("@ID", MedicalServiceID);
            SqlDataReader reader2;
            try
            {
                con.Open();
                reader2 = cmd.ExecuteReader();
                reader2.Read();
                txtMedicalServiceView.Text = reader2["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectLabOrder1";
            cmd.Parameters.AddWithValue("@ID", LabOrderID);
            SqlDataReader reader3;
            try
            {
                con.Open();
                reader3 = cmd.ExecuteReader();
                reader3.Read();
                txtDateView.Text = reader3["Date"].ToString();
                DID = reader3["DID"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectDoctor1";
            cmd.Parameters.AddWithValue("@ID", DID);
            SqlDataReader reader4;
            try
            {
                con.Open();
                reader4 = cmd.ExecuteReader();
                reader4.Read();
                txtDoctorView.Text = reader4["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }
        }

        protected void gridLabOrderDetails_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            var ID = gridLabOrderDetails.DataKeys[e.NewSelectedIndex].Value;
            string MedicalServiceID = "0";
            string LabOrderID = "0";
            string DID = "0";

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectLabOrderDetail1";
            cmd.Parameters.AddWithValue("@ID", ID);
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                txtIDView.Text = reader["ID"].ToString();
                txtResultView.Text = reader["Result"].ToString();
                MedicalServiceID = reader["MedicalServiceID"].ToString();
                LabOrderID = reader["LabOrderID"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectMedicalService1";
            cmd.Parameters.AddWithValue("@ID", MedicalServiceID);
            SqlDataReader reader2;
            try
            {
                con.Open();
                reader2 = cmd.ExecuteReader();
                reader2.Read();
                txtMedicalServiceView.Text = reader2["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectLabOrder1";
            cmd.Parameters.AddWithValue("@ID", LabOrderID);
            SqlDataReader reader3;
            try
            {
                con.Open();
                reader3 = cmd.ExecuteReader();
                reader3.Read();
                txtDateView.Text = reader3["Date"].ToString();
                DID = reader3["DID"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectDoctor1";
            cmd.Parameters.AddWithValue("@ID", DID);
            SqlDataReader reader4;
            try
            {
                con.Open();
                reader4 = cmd.ExecuteReader();
                reader4.Read();
                txtDoctorView.Text = reader4["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }
        }

        protected void btnSearch2_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSearchLabOrderDetail";
            cmd.Parameters.AddWithValue("@MedicalServiceID", dropMedicalServiceSearch.SelectedValue);
            cmd.Parameters.AddWithValue("@Result", txtResultSearch.Text.Trim());
            cmd.Parameters.AddWithValue("@LabOrderID", dropLabOrderIDSearch.SelectedValue);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                ad.Fill(table);
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            gridLabOrderDetails2.DataSource = table;
            gridLabOrderDetails2.DataBind();
            PopulateData();
        }
    }
}