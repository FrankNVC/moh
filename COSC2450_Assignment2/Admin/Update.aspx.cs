﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Linq;
using System.Text;
using System.Xml;
using System.IO;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Triggers;


namespace COSC2450_Assignment2.Admin
{
    public partial class Update : System.Web.UI.Page
    {

        private static IScheduler _scheduler;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            _scheduler = null;
            // start up scheduler
            // construct a factory
            ISchedulerFactory factory = new StdSchedulerFactory();
            // get a scheduler
            _scheduler = factory.GetScheduler();
            // start the scheduler
            _scheduler.Start();

            IJob myJob = new MyJob(); //This Constructor needs to be parameterless
            JobDetailImpl jobDetail = new JobDetailImpl("Job1", "Group1", myJob.GetType());
            SimpleTriggerImpl trigger = new SimpleTriggerImpl("Trigger1", null, DateTime.UtcNow, null, SimpleTriggerImpl.RepeatIndefinitely, TimeSpan.FromMinutes(1)); //run every minute

            _scheduler.Start();
            _scheduler.ScheduleJob(jobDetail, trigger);
        }

        class MyJob : IJob
        {
            public MyJob() { }

            private string conStr = WebConfigurationManager.ConnectionStrings["SqlService"].ConnectionString;
            private string HospitalList;
            private string DoctorList;

            public void Execute(IJobExecutionContext context)
            {
                COSC2450_Assignment2.ServiceReference1.AutoUpdateSoapClient ob = new COSC2450_Assignment2.ServiceReference1.AutoUpdateSoapClient();
                HospitalList = ob.HospitalList();
                DoctorList = ob.DoctorList();
                Update();
            }

            public void Update()
            {
                SqlConnection con = new SqlConnection(conStr);

                DataSet ds = new DataSet();
                ds.ReadXml("D:/hospital.xml");

                DataTable table = new DataTable();
                table = ds.Tables[0];

                foreach (DataRow dr in table.Rows)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spInsertHospital2";
                    cmd.Parameters.AddWithValue("@Name", dr["Name"].ToString());
                    cmd.Parameters.AddWithValue("@LicenseNo", dr["LicenseNo"].ToString());
                    cmd.Parameters.AddWithValue("@Address", dr["Address"].ToString());

                    con.Open();
                    cmd.ExecuteNonQuery();

                    con.Close();
                }

                ds = new DataSet();
                ds.ReadXml("D:/doctor.xml");

                table = new DataTable();
                table = ds.Tables[0];

                foreach (DataRow dr in table.Rows)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "spInsertDoctor2";
                    cmd.Parameters.AddWithValue("@Name", dr["Name"].ToString());
                    cmd.Parameters.AddWithValue("@Gender", dr["Gender"].ToString());
                    cmd.Parameters.AddWithValue("@DOB", dr["DOB"].ToString());
                    cmd.Parameters.AddWithValue("@LicenseNo", dr["LicenseNo"].ToString());
                    cmd.Parameters.AddWithValue("@Address", dr["Address"].ToString());

                    con.Open();
                    cmd.ExecuteNonQuery();

                    con.Close();
                }
            }
        }
    }
}