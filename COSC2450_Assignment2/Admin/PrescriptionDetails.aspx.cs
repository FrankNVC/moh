﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data.SqlClient;

namespace COSC2450_Assignment2
{
    public partial class PrescriptionDetails : System.Web.UI.Page
    {
        private string conStr = WebConfigurationManager.ConnectionStrings["SqlService"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateData();
            }
        }

        protected void gridPrescriptionDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridPrescriptionDetails.PageIndex = e.NewPageIndex;
            PopulateData();
        }

        protected void EditRow(object sender, GridViewEditEventArgs e)
        {
            gridPrescriptionDetails.EditIndex = e.NewEditIndex;
            PopulateData();
        }

        protected void CancelEditRow(object sender, GridViewCancelEditEventArgs e)
        {
            gridPrescriptionDetails.EditIndex = -1;
            PopulateData();
        }

        protected void DeleteRow(object sender, GridViewDeleteEventArgs e)
        {
            var ID = gridPrescriptionDetails.DataKeys[e.RowIndex].Value;

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spDeletePrescriptionDetail";
            cmd.Parameters.AddWithValue("@ID", ID);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                PopulateData();
            }
        }

        protected void UpdateRow(object sendedr, GridViewUpdateEventArgs e)
        {
            var ID = gridPrescriptionDetails.DataKeys[e.RowIndex].Value;

            GridViewRow row = gridPrescriptionDetails.Rows[e.RowIndex] as GridViewRow;
            TextBox tQuantity = row.FindControl("txtQuantity") as TextBox;
            TextBox tDose = row.FindControl("txtDose") as TextBox;
            TextBox tInstruction = row.FindControl("txtInstruction") as TextBox;
            DropDownList dropDrug = row.FindControl("dropDrug") as DropDownList;
            DropDownList dropPrescriptionID = row.FindControl("dropPrescriptionID") as DropDownList;

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spUpdatePrescriptionDetail";

            cmd.Parameters.AddWithValue("@ID", ID);
            cmd.Parameters.AddWithValue("@DrugID", dropDrug.SelectedValue);
            cmd.Parameters.AddWithValue("@PrescriptionID", dropPrescriptionID.SelectedValue);
            cmd.Parameters.AddWithValue("@Quantity", tQuantity.Text.Trim());
            cmd.Parameters.AddWithValue("@Dose", tDose.Text.Trim());
            cmd.Parameters.AddWithValue("@Instruction", tInstruction.Text.Trim());
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                gridPrescriptionDetails.EditIndex = -1;
                PopulateData();
            }
        }

        private void PopulateData()
        {
            dropPrescriptionDetail.Items.Clear();
            dropDrug2.Items.Clear();
            dropPrescriptionID2.Items.Clear();
            dropDrugSearch.Items.Clear();
            dropPrescriptionIDSearch.Items.Clear();

            dropDrugSearch.Items.Add("0");
            dropPrescriptionIDSearch.Items.Add("0");

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectPrescriptionDetails";
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                ad.Fill(table);
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally 
            { 
                con.Close();                
            }
            gridPrescriptionDetails.DataSource = table;
            gridPrescriptionDetails.DataBind();
           
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = reader["ID"] + ", " + reader["DrugID"] + ", " + reader["PrescriptionID"];
                    newItem.Value = reader["ID"].ToString();
                    dropPrescriptionDetail.Items.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }
            
            cmd.CommandText = "spSelectDrugs";
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = reader["ID"] + ", " + reader["Name"];
                    newItem.Value = reader["ID"].ToString();
                    dropDrug2.Items.Add(newItem);
                    dropDrugSearch.Items.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally 
            { 
                con.Close(); 
            }

            cmd.CommandText = "spSelectPrescriptions";
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = reader["ID"] + ", " + reader["Date"];
                    newItem.Value = reader["ID"].ToString();
                    dropPrescriptionID2.Items.Add(newItem);
                    dropPrescriptionIDSearch.Items.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally 
            { 
                con.Close(); 
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spInsertPrescriptionDetail";
            cmd.Parameters.AddWithValue("@DrugID", dropDrug2.SelectedValue);
            cmd.Parameters.AddWithValue("@PrescriptionID", dropPrescriptionID2.SelectedValue);
            cmd.Parameters.AddWithValue("@Quantity", txtQuantity2.Text);
            cmd.Parameters.AddWithValue("@Dose", txtDose2.Text);
            cmd.Parameters.AddWithValue("@Instruction", txtInstruction2.Text);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                Response.Write("<script language='javascript'>alert('1 record added.');</script");
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                PopulateData();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string DrugID = "0";
            string PrescriptionID = "0";
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectPrescriptionDetail1";
            cmd.Parameters.AddWithValue("@ID", dropPrescriptionDetail.SelectedValue);
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                txtIDView.Text = reader["ID"].ToString();
                txtQuantityView.Text = reader["Quantity"].ToString();
                txtDoseView.Text = reader["Dose"].ToString();
                txtInstructionView.Text = reader["Instruction"].ToString();
                DrugID = reader["DrugID"].ToString();
                PrescriptionID = reader["PrescriptionID"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem.'" + ex.ToString() + ");</script");
            }
            finally
            {
                con.Close();                
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectDrug1";
            cmd.Parameters.AddWithValue("@ID", DrugID);
            SqlDataReader reader2;
            try
            {
                con.Open();
                reader2 = cmd.ExecuteReader();
                reader2.Read();
                txtDrugView.Text = reader2["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem.'" + ex.ToString() + ");</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectPrescription1";
            cmd.Parameters.AddWithValue("@ID", PrescriptionID);
            SqlDataReader reader3;
            try
            {
                con.Open();
                reader3 = cmd.ExecuteReader();
                reader3.Read();
                txtDateView.Text = reader3["Date"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem.'" + ex.ToString() + ");</script");
            }
            finally
            {
                con.Close();
            }
        }

        protected void gridPrescriptionDetails_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            var ID = gridPrescriptionDetails.DataKeys[e.NewSelectedIndex].Value;
            string DrugID = "0";
            string PrescriptionID = "0";

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectPrescriptionDetail1";
            cmd.Parameters.AddWithValue("@ID", ID);
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                txtIDView.Text = reader["ID"].ToString();
                txtQuantityView.Text = reader["Quantity"].ToString();
                txtDoseView.Text = reader["Dose"].ToString();
                txtInstructionView.Text = reader["Instruction"].ToString();
                DrugID = reader["DrugID"].ToString();
                PrescriptionID = reader["PrescriptionID"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem.'" + ex.ToString() + ");</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectDrug1";
            cmd.Parameters.AddWithValue("@ID", DrugID);
            SqlDataReader reader2;
            try
            {
                con.Open();
                reader2 = cmd.ExecuteReader();
                reader2.Read();
                txtDrugView.Text = reader2["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem.'" + ex.ToString() + ");</script");
            }
            finally
            {
                con.Close();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectPrescription1";
            cmd.Parameters.AddWithValue("@ID", PrescriptionID);
            SqlDataReader reader3;
            try
            {
                con.Open();
                reader3 = cmd.ExecuteReader();
                reader3.Read();
                txtDateView.Text = reader3["Date"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem.'" + ex.ToString() + ");</script");
            }
            finally
            {
                con.Close();
            }
        }

        protected void btnSearch2_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSearchPrescriptionDetail";
            cmd.Parameters.AddWithValue("@DrugID", dropDrugSearch.SelectedValue);
            cmd.Parameters.AddWithValue("@PrescriptionID", dropPrescriptionIDSearch.SelectedValue);
            cmd.Parameters.AddWithValue("@Quantity", txtQuantitySearch.Text.Trim());
            cmd.Parameters.AddWithValue("@Dose", txtDoseSearch.Text.Trim());
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                ad.Fill(table);
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            gridPrescriptionDetails2.DataSource = table;
            gridPrescriptionDetails2.DataBind();
            PopulateData();
        }
    }
}