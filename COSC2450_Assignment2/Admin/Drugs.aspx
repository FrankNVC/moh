﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Drugs.aspx.cs" Inherits="COSC2450_Assignment2.Drugs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="title">
        <h2>This is the Drug Page.</h2>
    </div>
    <cc1:Accordion ID="Accordion1" runat="server" HeaderCssClass="accHeader" HeaderSelectedCssClass="accHeader" ContentCssClass="accContent">
        <Panes>
            <cc1:AccordionPane ID="AccordionPane1" runat="server">
                <Header> View All Drugs</Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gridDrugs" runat="server" CellSpacing="2" AutoGenerateColumns="False" AllowPaging="True" AllowSorting="True" DataKeyNames="ID"
                                ForeColor="#333333" GridLines="None" DataKeyName="ID" Width="600px" OnPageIndexChanging="gridDrugs_PageIndexChanging"
                                OnRowEditing="EditRow" OnRowCancelingEdit="CancelEditRow" OnRowUpdating="UpdateRow" OnRowDeleting="DeleteRow" OnSelectedIndexChanging="gridDrugs_SelectedIndexChanging">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <Columns>
                                    <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID" />
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <%# Eval("Name") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtName" runat="server" Text='<%# Eval("Name") %>' />
                                            <asp:RequiredFieldValidator ID="ReqFieldValName" runat="server" ControlToValidate="txtName" ValidationGroup="update" ErrorMessage="Input Name" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Generic">
                                        <ItemTemplate>
                                            <%# Eval("Generic") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtGeneric" runat="server" Text='<%# Eval("Generic") %>' />
                                            <asp:RequiredFieldValidator ID="ReqFieldValGeneric" runat="server" ControlToValidate="txtGeneric" ValidationGroup="update" ErrorMessage="Input Generic" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Unit">
                                        <ItemTemplate>
                                            <%# Eval("Unit") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtUnit" runat="server" Text='<%# Eval("Unit") %>' size="10" />
                                            <asp:RequiredFieldValidator ID="ReqFieldValUnit" runat="server" ControlToValidate="txtUnit" ValidationGroup="update" ErrorMessage="Input Unit" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Price">
                                        <ItemTemplate>
                                            <%# Eval("Price", "{0:$#,###.##}") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtPrice" runat="server" Text='<%# Eval("Price", "{0:$#,###.##}") %>' size="5" />
                                            <asp:RequiredFieldValidator ID="ReqFieldValPrice" runat="server" ControlToValidate="txtPrice" ValidationGroup="update" ErrorMessage="Input Price" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Group">
                                        <ItemTemplate>
                                            <%# Eval("GroupID") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="dropGroup" runat="server" AutoPostBack="True" DataSourceID="sourceGroups" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                            <cc1:ListSearchExtender ID="dropGroup_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropGroup" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                            </cc1:ListSearchExtender>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="Edit" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="lnkUpdate" runat="server" Text="Update" CommandName="Update" />
                                            <asp:LinkButton ID="lnkCancel" runat="server" Text="Cancel" CommandName="Cancel" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <span onclick="return confirm('Are you sure to delete?')">
                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" ForeColor="Red" CommandName="Delete" />
                                            </span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <PagerStyle BackColor="#284755" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="true" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284755" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                            <asp:Panel ID="pnlDrug" runat="server">
                                <div>
                                    <h2>Drug</h2>
                                </div>
                                <div class="form">
                                    <fieldset>
                                        <dl>
                                            <dt>
                                                <label>ID:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtIDView" runat="server" size="5"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Name:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtNameView" runat="server" size="30"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Generic:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtGenericView" runat="server" size="20"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Unit</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtUnitView" runat="server" size="10"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Price:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtPriceView" runat="server" size="10"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Group:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtGroupView" runat="server" size="30"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <asp:DropDownList ID="dropDrug" runat="server" AutoPostBack="True"></asp:DropDownList>
                                                <cc1:ListSearchExtender ID="dropDrug_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropDrug" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                            </dt>
                                        </dl>
                                        <dl>
                                            <dt></dt>
                                            <dd>
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                                            </dd>
                                        </dl>
                                    </fieldset>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlSearch" runat="server">
                                <asp:TextBox ID="txtNameSearch" runat="server" Text="Name"></asp:TextBox>
                                <asp:TextBox ID="txtGenericSearch" runat="server" Text="Generic"></asp:TextBox>
                                <asp:TextBox ID="txtUnitSearch" runat="server" Text="Unit"></asp:TextBox>
                                <asp:TextBox ID="txtPriceSearch" runat="server" Text="Price"></asp:TextBox>                                
                                <asp:DropDownList ID="dropDrugGroupSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropDrugGroupSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropDrugGroupSearch" PromptText="Type to Search" IsSorted="true" PromptPosition="Bottom" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:Button ID="btnSearch2" runat="server" Text="Search Many" OnClick="btnSearch2_Click" />
                            </asp:Panel>
                            <asp:GridView ID="gridDrugs2" runat="server" CellSpacing="2" DataKeyNames="ID"
                                ForeColor="#333333" GridLines="None" Width="600px">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <PagerStyle BackColor="#284755" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="true" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284755" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </cc1:AccordionPane>
            <cc1:AccordionPane ID="AccordionPane2" runat="server">
                <Header>Insert new Drug</Header>
                <Content>
                    <asp:Panel ID="Panel1" runat="server">
                        <div>
                            <h2>Insert Drug Form</h2>
                        </div>
                        <div class="form">
                            <fieldset>
                                <dl>
                                    <dt>
                                        <label>Drug Name:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtName2" runat="server" size="30"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ReqFieldValName2" runat="server" ControlToValidate="txtName2" ValidationGroup="insert" ErrorMessage="Input Name"></asp:RequiredFieldValidator>
                                        <div>
                                            <asp:RegularExpressionValidator ID="RegExValName2" runat="server" ControlToValidate="txtName2" ValidationGroup="insert" ValidationExpression="^(\w+)(\s\w+)*$" ErrorMessage="Inputted Name not Valid"></asp:RegularExpressionValidator>
                                        </div>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Generics:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtGeneric2" runat="server" size="20"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ReqFieldValGeneric2" runat="server" ControlToValidate="txtGeneric2" ValidationGroup="insert" ErrorMessage="Input Generic"></asp:RequiredFieldValidator>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Unit:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtUnit2" runat="server" size="30"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ReqFieldValUnit2" runat="server" ControlToValidate="txtUnit2" ValidationGroup="insert" ErrorMessage="Input Unit"></asp:RequiredFieldValidator>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Price:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtPrice2" runat="server" size="30"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ReqFieldValPrice2" runat="server" ControlToValidate="txtPrice2" ValidationGroup="insert" ErrorMessage="Input Price"></asp:RequiredFieldValidator>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Group:</label></dt>
                                    <dd>
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="dropGroup2" runat="server" AutoPostBack="True"></asp:DropDownList>
                                                <cc1:ListSearchExtender ID="dropGroup_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropGroup2" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt></dt>
                                    <dd>
                                        <asp:Button ID="btnSave" CssClass="bt_green" runat="server" Text="Add new Drug" BackColor="Lime" CausesValidation="true" ValidationGroup="insert" OnClick="btnSave_Click" />
                                    </dd>
                                </dl>
                            </fieldset>
                        </div>
                    </asp:Panel>
                </Content>
            </cc1:AccordionPane>
        </Panes>
    </cc1:Accordion>
    <asp:SqlDataSource ID="sourceGroups" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM DrugGroups"></asp:SqlDataSource>
</asp:Content>
