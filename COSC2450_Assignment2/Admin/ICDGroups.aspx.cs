﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data.SqlClient;

namespace COSC2450_Assignment2
{
    public partial class ICDGroups : System.Web.UI.Page
    {
        private string conStr = WebConfigurationManager.ConnectionStrings["SqlService"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateData();
            }
        }

        protected void gridICDGroups_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gridICDGroups.PageIndex = e.NewPageIndex;
            PopulateData();
        }

        protected void EditRow(object sender, GridViewEditEventArgs e)
        {
            gridICDGroups.EditIndex = e.NewEditIndex;
            PopulateData();
        }

        protected void CancelEditRow(object sender, GridViewCancelEditEventArgs e)
        {
            gridICDGroups.EditIndex = -1;
            PopulateData();
        }

        protected void DeleteRow(object sender, GridViewDeleteEventArgs e)
        {

            var ID = gridICDGroups.DataKeys[e.RowIndex].Value;

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spDeleteICDGroup";
            cmd.Parameters.AddWithValue("@ID", ID);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                PopulateData();
            }
        }

        protected void UpdateRow(object sendedr, GridViewUpdateEventArgs e)
        {

            var ID = gridICDGroups.DataKeys[e.RowIndex].Value;

            GridViewRow row = gridICDGroups.Rows[e.RowIndex] as GridViewRow;
            TextBox tName = row.FindControl("txtName") as TextBox;

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spUpdateICDGroup";

            cmd.Parameters.AddWithValue("@ID", ID);
            cmd.Parameters.AddWithValue("@Name", tName.Text.Trim());
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                gridICDGroups.EditIndex = -1;
                PopulateData();
            }
        }

        private void PopulateData()
        {
            dropICDGroup.Items.Clear();

            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectICDGroups";
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                ad.Fill(table);
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                gridICDGroups.DataSource = table;
                gridICDGroups.DataBind();
            }

            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectICDGroups";
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ListItem newItem = new ListItem();
                    newItem.Text = reader["ID"] + ", " + reader["Name"];
                    newItem.Value = reader["ID"].ToString();
                    dropICDGroup.Items.Add(newItem);
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spInsertICDGroup";
            cmd.Parameters.AddWithValue("@Name", txtName2.Text.Trim());
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                Response.Write("<script language='javascript'>alert('1 record added.');</script");
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '+" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
                PopulateData();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectICDGroup1";
            cmd.Parameters.AddWithValue("@ID", dropICDGroup.SelectedValue);
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                txtIDView.Text = reader["ID"].ToString();
                txtNameView.Text = reader["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }
        }

        protected void gridICDGroups_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            var ID = gridICDGroups.DataKeys[e.NewSelectedIndex].Value;

            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectICDGroup1";
            cmd.Parameters.AddWithValue("@ID", ID);
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                reader.Read();
                txtIDView.Text = reader["ID"].ToString();
                txtNameView.Text = reader["Name"].ToString();
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }
        }

        protected void btnSearch2_Click(object sender, EventArgs e)
        {
            DataTable table = new DataTable();
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSearchICDGroup";
            cmd.Parameters.AddWithValue("@Name", txtNameSearch.Text.Trim());
            SqlDataAdapter ad = new SqlDataAdapter(cmd);

            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                ad.Fill(table);
            }
            catch (Exception ex)
            {
                Response.Write("<script language='javascript'>alert('Connection Problem. '" + ex.ToString() + "');</script");
            }
            finally
            {
                con.Close();
            }

            gridICDGroups2.DataSource = table;
            gridICDGroups2.DataBind();
            PopulateData();
        }
    }
}