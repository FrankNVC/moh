﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="MedicalServices.aspx.cs" Inherits="COSC2450_Assignment2.MedicalServices" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="title">
        <h2>This is the Medical Service Page.</h2>
    </div>
    <cc1:Accordion ID="Accordion1" runat="server" HeaderCssClass="accHeader" HeaderSelectedCssClass="accHeader" ContentCssClass="accContent">
        <Panes>
            <cc1:AccordionPane ID="AccordionPane1" runat="server">
                <Header> View All Medical Services</Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gridMedicalServices" runat="server" CellSpacing="2" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="ID"
                                ForeColor="#333333" GridLines="None" Width="600px" OnPageIndexChanging="gridMedicalServices_PageIndexChanging"
                                OnRowEditing="EditRow" OnRowCancelingEdit="CancelEditRow" OnRowUpdating="UpdateRow" OnRowDeleting="DeleteRow" OnSelectedIndexChanging="gridMedicalServices_SelectedIndexChanging">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <Columns>
                                    <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID" />
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <%# Eval("Name") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtName" runat="server" Text='<%# Eval("Name") %>' />
                                            <asp:RequiredFieldValidator ID="ReqFieldValName" runat="server" ControlToValidate="txtName" ValidationGroup="update" ErrorMessage="Input Name" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Price">
                                        <ItemTemplate>
                                            <%# Eval("Price", "{0:$#,###.##}") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtPrice" runat="server" Text='<%# Eval("Price") %>' />
                                            <asp:RequiredFieldValidator ID="ReqFieldValPrice" runat="server" ControlToValidate="txtPrice" ValidationGroup="update" ErrorMessage="Input Price" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Service Group">
                                        <ItemTemplate>
                                            <%# Eval("GroupID") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="dropMedicalServiceGroup" runat="server" AutoPostBack="True" DataSourceID="sourceMedicalServiceGroups" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                            <cc1:ListSearchExtender ID="dropMedicalServiceGroup_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropMedicalServiceGroup" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                            </cc1:ListSearchExtender>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="Edit" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="lnkUpdate" runat="server" Text="Update" CommandName="Update" CausesValidation="true" ValidationGroup="update" />
                                            <asp:LinkButton ID="lnkCancel" runat="server" Text="Cancel" CommandName="Cancel" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <span onclick="return confirm('Are you sure to delete?')">
                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" ForeColor="Red" CommandName="Delete" />
                                            </span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <PagerStyle BackColor="#284755" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="true" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284755" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                            <asp:Panel ID="pnlMedicalService" runat="server">
                                <div>
                                    <h2>Medical Service</h2>
                                </div>
                                <div class="form">
                                    <fieldset>
                                        <dl>
                                            <dt>
                                                <label>ID:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtIDView" runat="server" size="5"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Name:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtNameView" runat="server" size="50"></asp:TextBox>
                                            </dd>
                                        </dl>                                        
                                        <dl>
                                            <dt>
                                                <label>Price:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtPriceView" runat="server" size="20"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Group:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtGroupView" runat="server" size="20"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <asp:DropDownList ID="dropMedicalService" runat="server" AutoPostBack="True"></asp:DropDownList>
                                                <cc1:ListSearchExtender ID="dropMedicalService_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropMedicalService" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                            </dt>                                            
                                        </dl>
                                        <dl>
                                            <dt></dt>
                                            <dd>
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                                            </dd>
                                        </dl>
                                    </fieldset>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlSearch" runat="server">
                                <asp:TextBox ID="txtNameSearch" runat="server" Text="Name"></asp:TextBox>                                
                                <asp:TextBox ID="txtPriceSearch" runat="server" Text="Price"></asp:TextBox>
                                <asp:DropDownList ID="dropMedicalGroupSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropMedicalGroupSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropMedicalGroupSearch" PromptText="Type to Search" IsSorted="true" PromptPosition="Bottom" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:Button ID="btnSearch2" runat="server" Text="Search Many" OnClick="btnSearch2_Click" />
                            </asp:Panel>
                            <asp:GridView ID="gridMedicalServices2" runat="server" CellSpacing="2" DataKeyNames="ID"
                                ForeColor="#333333" GridLines="None" Width="600px">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <PagerStyle BackColor="#284755" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="true" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284755" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </cc1:AccordionPane>
            <cc1:AccordionPane ID="AccordionPane2" runat="server">
                <Header>Insert new Medical Service</Header>
                <Content>
                    <asp:Panel ID="Panel1" runat="server">
                        <div>
                            <h2>Insert Medical Service Form</h2>
                        </div>
                        <div class="form">
                            <fieldset>
                                <dl>
                                    <dt>
                                        <label>Medical Service Name:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtName2" runat="server" size="30"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ReqFieldValName2" runat="server" ControlToValidate="txtName2" ValidationGroup="insert" ErrorMessage="Input Name"></asp:RequiredFieldValidator>
                                        <div>
                                            <asp:RegularExpressionValidator ID="RegExValName2" runat="server" ControlToValidate="txtName2" ValidationGroup="insert" ValidationExpression="^(\w+)(\s\w+)*$" ErrorMessage="Inputted Name not Valid"></asp:RegularExpressionValidator>
                                        </div>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Price:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtPrice2" runat="server" size="20"></asp:TextBox>
                                        <cc1:MaskedEditExtender ID="MEEPrice" runat="server" TargetControlID="txtPrice2" Mask="99,999.99" MaskType="Number" MessageValidatorTip="true"></cc1:MaskedEditExtender>
                                        <cc1:MaskedEditValidator ID="MEVPrice" runat="server" ControlToValidate="txtPrice2" ValidationGroup="insert" ControlExtender="MEEPrice" IsValidEmpty="false" EmptyValueMessage="Input Price" InvalidValueMessage="Inputted Price not Valid"></cc1:MaskedEditValidator>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Medical Service Group:</label></dt>
                                    <dd>
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="dropMedicalServiceGroup2" runat="server" AutoPostBack="True"></asp:DropDownList>
                                                <cc1:ListSearchExtender ID="dropMedicalServiceGroup_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropMedicalServiceGroup2" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt></dt>
                                    <dd>
                                        <asp:Button ID="btnSave" CssClass="bt_green" runat="server" Text="Add new MedicalService" BackColor="Lime" CausesValidation="true" ValidationGroup="insert" OnClick="btnSave_Click" />
                                    </dd>
                                </dl>
                            </fieldset>
                        </div>
                    </asp:Panel>
                </Content>
            </cc1:AccordionPane>
        </Panes>
    </cc1:Accordion>
    <asp:SqlDataSource ID="sourceMedicalServiceGroups" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM MedicalServiceGroups"></asp:SqlDataSource>
</asp:Content>
