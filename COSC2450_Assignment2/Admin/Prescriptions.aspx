﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Prescriptions.aspx.cs" Inherits="COSC2450_Assignment2.Prescriptions" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="title">
        <h2>This is the Prescription Page.</h2>
    </div>
    <cc1:Accordion ID="Accordion1" runat="server" HeaderCssClass="accHeader" HeaderSelectedCssClass="accHeader" ContentCssClass="accContent">
        <Panes>
            <cc1:AccordionPane ID="AccordionPane1" runat="server">
                <Header> View All Prescriptions</Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gridPrescriptions" runat="server" CellSpacing="2" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="ID"
                                ForeColor="#333333" GridLines="None" Width="600px" OnPageIndexChanging="gridPrescriptions_PageIndexChanging"
                                OnRowEditing="EditRow" OnRowCancelingEdit="CancelEditRow" OnRowUpdating="UpdateRow" OnRowDeleting="DeleteRow" OnSelectedIndexChanging="gridPrescriptions_SelectedIndexChanging">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <Columns>
                                    <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID" />
                                    <asp:TemplateField HeaderText="Issued Date">
                                        <ItemTemplate>
                                            <%# Eval("Date", "{0:MM/dd/yyyy}") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtDate" runat="server" Text='<%# Eval("Date", "{0:MM/dd/yyyy}") %>' size="10" /><a runat="server" id="hrefDate">Calendar</a>
                                            <cc1:CalendarExtender ID="CalendarExt" runat="server" Format="MM/dd/yyyy" TargetControlID="txtDate" PopupButtonID="hrefDate" Enabled="true"></cc1:CalendarExtender>
                                            <div>
                                                <cc1:MaskedEditExtender ID="MEEDate" runat="server" TargetControlID="txtDate" Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true"></cc1:MaskedEditExtender>
                                                <cc1:MaskedEditValidator ID="MEVDate" runat="server" ControlToValidate="txtDate" ValidationGroup="update" ControlExtender="MEEDate" IsValidEmpty="false" EmptyValueMessage="Input Date" InvalidValueMessage="Inputted Date not Valid"></cc1:MaskedEditValidator>
                                            </div>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Doctor">
                                        <ItemTemplate>
                                            <%# Eval("DID") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="dropDoctor" runat="server" AutoPostBack="True" DataSourceID="sourceDoctors" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                            <cc1:ListSearchExtender ID="dropDoctor_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropDoctor" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                            </cc1:ListSearchExtender>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="Edit" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="lnkUpdate" runat="server" Text="Update" CommandName="Update" CausesValidation="true" ValidationGroup="update" />
                                            <asp:LinkButton ID="lnkCancel" runat="server" Text="Cancel" CommandName="Cancel" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <span onclick="return confirm('Are you sure to delete?')">
                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" ForeColor="Red" CommandName="Delete" />
                                            </span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <PagerStyle BackColor="#284755" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="true" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284755" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                            <asp:Panel ID="pnlPrescription" runat="server">
                                <div>
                                    <h2>Prescription</h2>
                                </div>
                                <div class="form">
                                    <fieldset>
                                        <dl>
                                            <dt>
                                                <label>ID:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtIDView" runat="server" size="5"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Date:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtDateView" runat="server" size="20"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Doctor Ordered:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtDoctorView" runat="server" size="30"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <asp:DropDownList ID="dropPrescription" runat="server" AutoPostBack="True" DataTextField="Date" DataValueField="ID"></asp:DropDownList>
                                                <cc1:ListSearchExtender ID="dropPrescription_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropPrescription" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender></dt>                                            
                                        </dl>
                                        <dl>
                                            <dt></dt>
                                            <dd>
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                                            </dd>
                                        </dl>
                                    </fieldset>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlSearch" runat="server">
                                <asp:TextBox ID="txtDateSearch" runat="server" Text="Date"></asp:TextBox><a runat="server" id="hrefDateSearch">Calendar</a>
                                <cc1:CalendarExtender ID="CalendarExt3" runat="server" Format="MM/dd/yyyy" TargetControlID="txtDateSearch" PopupButtonID="hrefDateSearch" Enabled="true"></cc1:CalendarExtender>
                                <cc1:MaskedEditExtender ID="MEEDateSearch" runat="server" TargetControlID="txtDateSearch" Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true"></cc1:MaskedEditExtender>
                                <cc1:MaskedEditValidator ID="MEVDateSearch" runat="server" ControlToValidate="txtDateSearch" ControlExtender="MEEDateSearch" IsValidEmpty="true" ></cc1:MaskedEditValidator>
                                <asp:DropDownList ID="dropDoctorSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropDoctorSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropDoctorSearch" PromptText="Type to Search" IsSorted="true" PromptPosition="Bottom" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:Button ID="btnSearch2" runat="server" Text="Search Many" OnClick="btnSearch2_Click" />
                            </asp:Panel>
                            <asp:GridView ID="gridPrescriptions2" runat="server" CellSpacing="2" DataKeyNames="ID"
                                ForeColor="#333333" GridLines="None" Width="600px">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <PagerStyle BackColor="#284755" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="true" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284755" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </cc1:AccordionPane>
            <cc1:AccordionPane ID="AccordionPane2" runat="server">
                <Header>Insert new Prescription</Header>
                <Content>
                    <asp:Panel ID="Panel1" runat="server">
                        <div>
                            <h2>Insert Prescription Form</h2>
                        </div>
                        <div class="form">
                            <fieldset>
                                <dl>
                                    <dt>
                                        <label>Date:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtDate2" runat="server" size="20"></asp:TextBox>(MM/dd/yyyy)<a runat="server" id="hrefDate2">Calendar</a>
                                        <cc1:CalendarExtender ID="CalendarExt2" runat="server" Format="MM/dd/yyyy" TargetControlID="txtDate2" PopupButtonID="hrefDate2" Enabled="true"></cc1:CalendarExtender>
                                        <div>
                                            <cc1:MaskedEditExtender ID="MEEDate2" runat="server" TargetControlID="txtDate2" Mask="99/99/9999" MaskType="Date" MessageValidatorTip="true"></cc1:MaskedEditExtender>
                                            <cc1:MaskedEditValidator ID="MEVDate2" runat="server" ControlToValidate="txtDate2" ValidationGroup="insert" ControlExtender="MEEDate2" IsValidEmpty="false" EmptyValueMessage="Input Date" InvalidValueMessage="Inputted Date not Valid"></cc1:MaskedEditValidator>
                                        </div>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Doctor:</label></dt>
                                    <dd>
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="dropDoctor2" runat="server" AutoPostBack="True"></asp:DropDownList>
                                                <cc1:ListSearchExtender ID="dropDoctor_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropDoctor2" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt></dt>
                                    <dd>
                                        <asp:Button ID="btnSave" CssClass="bt_green" runat="server" Text="Add new Prescription" BackColor="Lime" CausesValidation="true" ValidationGroup="insert" OnClick="btnSave_Click" />
                                    </dd>
                                </dl>
                                <dl>
                                    <h2>Insert Prescription Detail Form</h2>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Drug:</label></dt>
                                    <dd>
                                        <asp:DropDownList ID="dropDrug" runat="server" AutoPostBack="True"></asp:DropDownList>
                                        <cc1:ListSearchExtender ID="Drug_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropDrug" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                        </cc1:ListSearchExtender>                                        
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Quantity:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtQuantity" runat="server" size="20"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ReqFieldValQuantity" runat="server" ControlToValidate="txtQuantity" ValidationGroup="insertDetail" ErrorMessage="Input Quantity"></asp:RequiredFieldValidator>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Dose:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtDose" runat="server" size="20"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ReqFieldValDose" runat="server" ControlToValidate="txtDose" ValidationGroup="insertDetail" ErrorMessage="Input Dose"></asp:RequiredFieldValidator>-
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Instruction:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtInstruction" runat="server" size="20"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ReFieldValInstruction" runat="server" ControlToValidate="txtInstruction" ValidationGroup="insertDetail" ErrorMessage="Input Instruction"></asp:RequiredFieldValidator>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Prescription:</label></dt>
                                    <dd>
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="dropPrescriptionID" runat="server" AutoPostBack="True" DataTextField="ID" DataValueField="ID" Width="150px"></asp:DropDownList>
                                        <cc1:ListSearchExtender ID="dropPrescriptionID_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropPrescriptionID" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                        </cc1:ListSearchExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt></dt>
                                    <dd>
                                        <asp:Button ID="btnOk" CssClass="bt_green" runat="server" Text="Add new Prescription Detail" BackColor="Lime" CausesValidation="true" ValidationGroup="insertDetail" OnClick="btnOK_Click" />                                        
                                    </dd>
                                </dl>
                            </fieldset>
                        </div>
                    </asp:Panel>                    
                </Content>
            </cc1:AccordionPane>            
        </Panes>
    </cc1:Accordion>
    <asp:SqlDataSource ID="sourceDoctors" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM Doctors"></asp:SqlDataSource>
</asp:Content>
