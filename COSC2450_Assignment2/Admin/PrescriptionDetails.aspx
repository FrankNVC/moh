﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="PrescriptionDetails.aspx.cs" Inherits="COSC2450_Assignment2.PrescriptionDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="title">
        <h2>This is the Prescription Detail Page.</h2>
    </div>
    <cc1:Accordion ID="Accordion1" runat="server" HeaderCssClass="accHeader" HeaderSelectedCssClass="accHeader" ContentCssClass="accContent">
        <Panes>
            <cc1:AccordionPane ID="AccordionPane1" runat="server">
                <Header> View All Prescription Details</Header>
                <Content>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="gridPrescriptionDetails" runat="server" CellSpacing="2" AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="ID"
                                ForeColor="#333333" GridLines="None" Width="600px" OnPageIndexChanging="gridPrescriptionDetails_PageIndexChanging"
                                OnRowEditing="EditRow" OnRowCancelingEdit="CancelEditRow" OnRowUpdating="UpdateRow" OnRowDeleting="DeleteRow" OnSelectedIndexChanging="gridPrescriptionDetails_SelectedIndexChanging">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <Columns>
                                    <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" SortExpression="ID" />                                    
                                    <asp:TemplateField HeaderText="Drug">
                                        <ItemTemplate>
                                            <%# Eval("DrugID") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="dropDrug" runat="server" AutoPostBack="True" DataSourceID="sourceDrugs" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                            <cc1:ListSearchExtender ID="Drug_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropDrug" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                            </cc1:ListSearchExtender>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Prescription ID">
                                        <ItemTemplate>
                                            <%# Eval("PrescriptionID") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="dropPrescriptionID" runat="server" AutoPostBack="True" DataSourceID="sourcePrescriptionIDs" DataTextField="Date" DataTextFormatString="{0:dd/MM/yyyy}" DataValueField="ID"></asp:DropDownList>
                                            <cc1:ListSearchExtender ID="PrescriptionID_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropPrescriptionID" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                            </cc1:ListSearchExtender>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Quantity">
                                        <ItemTemplate>
                                            <%# Eval("Quantity") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtQuantity" runat="server" Text='<%# Eval("Quantity") %>' size="5" />
                                            <asp:RequiredFieldValidator ID="ReqFieldValQuantity" runat="server" ControlToValidate="txtQuantity" ValidationGroup="update" ErrorMessage="Input Quantity" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dose">
                                        <ItemTemplate>
                                            <%# Eval("Dose") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtDose" runat="server" Text='<%# Eval("Dose") %>' size="5" />
                                            <asp:RequiredFieldValidator ID="ReqFieldValDose" runat="server" ControlToValidate="txtDose" ValidationGroup="update" ErrorMessage="Input Dose" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Instruction">
                                        <ItemTemplate>
                                            <%# Eval("Instruction") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtInstruction" runat="server" Text='<%# Eval("Instruction") %>' />
                                            <asp:RequiredFieldValidator ID="ReqFieldValInstruction" runat="server" ControlToValidate="txtInstruction" ValidationGroup="update" ErrorMessage="Input Instruction" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ButtonType="Button" ShowSelectButton="True" />
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="Edit" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="lnkUpdate" runat="server" Text="Update" CommandName="Update" CausesValidation="true" ValidationGroup="update" />
                                            <asp:LinkButton ID="lnkCancel" runat="server" Text="Cancel" CommandName="Cancel" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <span onclick="return confirm('Are you sure to delete?')">
                                                <asp:LinkButton ID="lnkDelete" runat="server" Text="Delete" ForeColor="Red" CommandName="Delete" />
                                            </span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <PagerStyle BackColor="#284755" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="true" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284755" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                            <asp:Panel ID="pnlPrescriptionDetail" runat="server">
                                <div>
                                    <h2>Prescription Detail</h2>
                                </div>
                                <div class="form">
                                    <fieldset>
                                        <dl>
                                            <dt>
                                                <label>ID:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtIDView" runat="server" size="5"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Date:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtDateView" runat="server" size="20"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Drug:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtDrugView" runat="server" size="30"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Quantity:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtQuantityView" runat="server" size="5"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Dose:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtDoseView" runat="server" size="5"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <label>Instruction:</label></dt>
                                            <dd>
                                                <asp:TextBox ID="txtInstructionView" runat="server" size="50"></asp:TextBox>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>
                                                <asp:DropDownList ID="dropPrescriptionDetail" runat="server" AutoPostBack="True" DataTextField="Name" DataValueField="ID"></asp:DropDownList></dt>
                                                <cc1:ListSearchExtender ID="dropPrescriptionDetail_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropPrescriptionDetail" IsSorted="true" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>                                            
                                        </dl>
                                        <dl>
                                            <dt></dt>
                                            <dd>                                                
                                                 <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" /><br />
                                            </dd>
                                        </dl>
                                    </fieldset>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="pnlSearch" runat="server">                                
                                <asp:DropDownList ID="dropDrugSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropDrugSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropDrugSearch" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:DropDownList ID="dropPrescriptionIDSearch" runat="server" AutoPostBack="True"></asp:DropDownList>
                                <cc1:ListSearchExtender ID="dropPrescriptionIDSearch_ListSearchExtender" runat="server" Enabled="True" TargetControlID="dropPrescriptionIDSearch" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                </cc1:ListSearchExtender>
                                <asp:TextBox ID="txtQuantitySearch" runat="server" Text="Quantity"></asp:TextBox>
                                <asp:TextBox ID="txtDoseSearch" runat="server" Text="Dose"></asp:TextBox>
                                <asp:Button ID="btnSearch2" runat="server" Text="Search Many" OnClick="btnSearch2_Click" />
                            </asp:Panel>
                            <asp:GridView ID="gridPrescriptionDetails2" runat="server" CellSpacing="2" DataKeyNames="ID"
                                ForeColor="#333333" GridLines="None" Width="600px">
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <PagerStyle BackColor="#284755" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="true" ForeColor="#333333" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <AlternatingRowStyle BackColor="White" ForeColor="#284755" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </Content>
            </cc1:AccordionPane>
            <cc1:AccordionPane ID="AccordionPane2" runat="server">
                <Header>Insert new Prescription Detail</Header>
                <Content>
                    <asp:Panel ID="Panel1" runat="server">
                        <div>
                            <h2>Insert Prescription Detail Form</h2>
                        </div>
                        <div class="form">
                            <fieldset>
                                <dl>
                                    <dt>
                                        <label>Drug:</label></dt>
                                    <dd>
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="dropDrug2" runat="server" AutoPostBack="True"></asp:DropDownList>
                                                <cc1:ListSearchExtender ID="dropDrug2_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropDrug2" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Prescription ID:</label></dt>
                                    <dd>
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="dropPrescriptionID2" runat="server" AutoPostBack="True"></asp:DropDownList>
                                                <cc1:ListSearchExtender ID="dropPrescriptionID2_ListSearchExtender" runat="server" Enabled="true" TargetControlID="dropPrescriptionID2" PromptText="Type to Search" IsSorted="true" PromptPosition="Top" QueryPattern="Contains">
                                                </cc1:ListSearchExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Quantity:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtQuantity2" runat="server" size="30"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ReqFieldValQuantity2" runat="server" ControlToValidate="txtQuantity2" ValidationGroup="insert" ErrorMessage="Input Quantity"></asp:RequiredFieldValidator>
                                    </dd>
                                </dl>
                                <dl>
                                    <dt>
                                        <label>Dose:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtDose2" runat="server" size="30"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ReqFieldValDose2" runat="server" ControlToValidate="txtDose2" ValidationGroup="insert" ErrorMessage="Input Dose"></asp:RequiredFieldValidator>
                                    </dd>
                                </dl>                   
                                <dl>
                                    <dt>
                                        <label>Instruction:</label></dt>
                                    <dd>
                                        <asp:TextBox ID="txtInstruction2" runat="server" size="30"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ReqFieldValInstruction2" runat="server" ControlToValidate="txtInstruction2" ValidationGroup="insert" ErrorMessage="Input Instruction"></asp:RequiredFieldValidator>
                                    </dd>
                                </dl>                                
                                <dl>
                                    <dt></dt>
                                    <dd>
                                        <asp:Button ID="btnSave" CssClass="bt_green" runat="server" Text="Add new PrescriptionDetail" BackColor="Lime" CausesValidation="true" ValidationGroup="insert" OnClick="btnSave_Click" />
                                    </dd>
                                </dl>
                            </fieldset>
                        </div>
                    </asp:Panel>
                </Content>
            </cc1:AccordionPane>           
        </Panes>
    </cc1:Accordion>
    <asp:SqlDataSource ID="sourcePrescriptionDetails" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM PrescriptionDetails"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sourcePrescriptionIDs" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM Prescriptions"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sourceDrugs" runat="server" ConnectionString="Data Source=windows-i9nl9kp;Initial Catalog=ERM;Integrated Security=True"
        ProviderName="System.Data.SqlClient" SelectCommand="SELECT * FROM Drugs"></asp:SqlDataSource>
</asp:Content>