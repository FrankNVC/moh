﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml.Linq;

namespace WebServiceConsumer
{
    /// <summary>
    /// Summary description for AutoUpdate
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AutoUpdate : System.Web.Services.WebService
    {
        private string conStr = "Data Source=Windows-i9nl9kp;Initial Catalog=MOH;Integrated Security=True";

        [WebMethod]
        public string HospitalList()
        {
            XElement.Parse(GetHospital()).Save("D:/hospital.xml");
            return File.ReadAllText("D:/hospital.xml");
        }

        [WebMethod]
        public string DoctorList()
        {
            XElement.Parse(GetDoctor()).Save("D:/doctor.xml");
            return File.ReadAllText("D:/doctor.xml");
        }
        
        public string GetHospital()
        {
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectHospitals";
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();

            con.Open();
            cmd.ExecuteNonQuery();
            ad.Fill(ds);

            con.Close();

            return ds.GetXml();
        }

        public string GetDoctor()
        {
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "spSelectDoctors";
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();

            con.Open();
            cmd.ExecuteNonQuery();
            ad.Fill(ds);

            con.Close();

            return ds.GetXml();
        }
    }
}
