USE [ERM]
GO

INSERT INTO [dbo].[PrescriptionDetails]
           ([DrugID]
           ,[PrescriptionID]
           ,[Quantity]
           ,[Dose]
           ,[Instruction])
     VALUES
           (1,
           1,
           20,
           2,
           'Use before meal')

INSERT INTO [dbo].[PrescriptionDetails]
           ([DrugID]
           ,[PrescriptionID]
           ,[Quantity]
           ,[Dose]
           ,[Instruction])
     VALUES
           (2,
           2,
           15,
           1,
           'Before breakfast')

INSERT INTO [dbo].[PrescriptionDetails]
           ([DrugID]
           ,[PrescriptionID]
           ,[Quantity]
           ,[Dose]
           ,[Instruction])
     VALUES
           (5,
           3,
           30,
           2,
           'Twice a day, before lunch and dinner')
GO

