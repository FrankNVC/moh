USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateLabOrderDetail]    Script Date: 9/3/2013 9:19:12 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdateLabOrderDetail]
	@ID int=0,
	@MedicalServiceID int=0,
	@Result nvarchar(10),
	@LabOrderID int=0
AS
	UPDATE LabOrderDetails SET MedicalServiceID=@MedicalServiceID, Result=@Result, LabOrderID=@LabOrderID WHERE ID=@ID
RETURN 0
GO

