USE [ERM]
GO

INSERT INTO [dbo].[Patients]
           ([Name]
           ,[Gender]
           ,[DOB]
           ,[Address])
     VALUES
           ('Pham Thi My Uyen',
		   'Female',
           '05-06-1987',
           '112 3 Thang 2')

INSERT INTO [dbo].[Patients]
           ([Name]
           ,[Gender]
           ,[DOB]
           ,[Address])
     VALUES
           ('Tran Thanh Tung',
		   'Male',
           '02-23-1987',
           '32 Ly Thuong Kiet')

INSERT INTO [dbo].[Patients]
           ([Name]
           ,[Gender]
           ,[DOB]
           ,[Address])
     VALUES
           ('Nguyen Nhu Thao',
		   'Female',
           '09-14-1987',
           '67 Hung Vuong')
GO