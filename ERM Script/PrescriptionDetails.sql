USE [ERM]
GO

/****** Object:  Table [dbo].[PrescriptionDetails]    Script Date: 9/3/2013 7:58:44 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PrescriptionDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DrugID] [int] NOT NULL,
	[PrescriptionID] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Dose] [int] NOT NULL,
	[Instruction] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_PrescriptionDetails2] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[PrescriptionDetails]  WITH CHECK ADD  CONSTRAINT [FK_PrescriptionDetails2_Prescriptions2] FOREIGN KEY([PrescriptionID])
REFERENCES [dbo].[Prescriptions] ([ID])
GO

ALTER TABLE [dbo].[PrescriptionDetails] CHECK CONSTRAINT [FK_PrescriptionDetails2_Prescriptions2]
GO

