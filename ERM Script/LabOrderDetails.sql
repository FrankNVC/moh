USE [ERM]
GO

/****** Object:  Table [dbo].[LabOrderDetails]    Script Date: 9/3/2013 7:57:29 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[LabOrderDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MedicalServiceID] [int] NOT NULL,
	[Result] [nvarchar](50) NOT NULL,
	[LabOrderID] [int] NOT NULL,
 CONSTRAINT [PK_LabOrderDetails2_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[LabOrderDetails]  WITH CHECK ADD  CONSTRAINT [FK_LabOrderDetails2_LabOrders2] FOREIGN KEY([LabOrderID])
REFERENCES [dbo].[LabOrders] ([ID])
GO

ALTER TABLE [dbo].[LabOrderDetails] CHECK CONSTRAINT [FK_LabOrderDetails2_LabOrders2]
GO

