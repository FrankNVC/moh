USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateDrug]    Script Date: 9/3/2013 9:18:19 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdateDrug]
	@ID int=0,
	@Name nvarchar(50),
	@Generic nvarchar(50),
	@Unit nvarchar(10),
	@Price money,
	@GroupID int=0
AS
	UPDATE Drugs SET Name=@Name, Generic=@Generic, Unit=@Unit, Price=@Price, GroupID=@GroupID WHERE ID=@ID
RETURN 0
GO

