USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateMedicalServiceGroup]    Script Date: 9/3/2013 9:19:32 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdateMedicalServiceGroup]
	@ID int=0,
	@Name nvarchar(50)
AS
	UPDATE MedicalServiceGroups SET Name=@Name WHERE ID=@ID
RETURN 0
GO

