USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spInsertPrescription]    Script Date: 9/3/2013 9:07:57 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spInsertPrescription]
	@Date date,
	@DID int=0

AS
	INSERT INTO Prescriptions VALUES(@Date,@DID)
RETURN 0
GO

