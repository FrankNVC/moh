USE [ERM]
GO

/****** Object:  Table [dbo].[Visits]    Script Date: 9/3/2013 7:59:03 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Visits](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [date] NOT NULL,
	[PatientID] [int] NOT NULL,
	[HospitalID] [int] NOT NULL,
	[DoctorID] [int] NOT NULL,
	[PrescriptionID] [int] NOT NULL,
	[LabOrderID] [int] NOT NULL,
	[ICDID] [int] NOT NULL,
	[Outcome] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_Visits2] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Visits]  WITH CHECK ADD  CONSTRAINT [FK_Visits2_Doctors2] FOREIGN KEY([DoctorID])
REFERENCES [dbo].[Doctors] ([ID])
GO

ALTER TABLE [dbo].[Visits] CHECK CONSTRAINT [FK_Visits2_Doctors2]
GO

ALTER TABLE [dbo].[Visits]  WITH CHECK ADD  CONSTRAINT [FK_Visits2_Hospitals2] FOREIGN KEY([HospitalID])
REFERENCES [dbo].[Hospitals] ([ID])
GO

ALTER TABLE [dbo].[Visits] CHECK CONSTRAINT [FK_Visits2_Hospitals2]
GO

ALTER TABLE [dbo].[Visits]  WITH CHECK ADD  CONSTRAINT [FK_Visits2_ICDs2] FOREIGN KEY([ICDID])
REFERENCES [dbo].[ICDs] ([ID])
GO

ALTER TABLE [dbo].[Visits] CHECK CONSTRAINT [FK_Visits2_ICDs2]
GO

ALTER TABLE [dbo].[Visits]  WITH CHECK ADD  CONSTRAINT [FK_Visits2_LabOrders2] FOREIGN KEY([LabOrderID])
REFERENCES [dbo].[LabOrders] ([ID])
GO

ALTER TABLE [dbo].[Visits] CHECK CONSTRAINT [FK_Visits2_LabOrders2]
GO

ALTER TABLE [dbo].[Visits]  WITH CHECK ADD  CONSTRAINT [FK_Visits2_Patients2] FOREIGN KEY([PatientID])
REFERENCES [dbo].[Patients] ([ID])
GO

ALTER TABLE [dbo].[Visits] CHECK CONSTRAINT [FK_Visits2_Patients2]
GO

ALTER TABLE [dbo].[Visits]  WITH CHECK ADD  CONSTRAINT [FK_Visits2_Prescriptions2] FOREIGN KEY([PrescriptionID])
REFERENCES [dbo].[Prescriptions] ([ID])
GO

ALTER TABLE [dbo].[Visits] CHECK CONSTRAINT [FK_Visits2_Prescriptions2]
GO

