USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spSearchPatient]    Script Date: 9/3/2013 8:07:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSearchPatient]
	@Name nvarchar(50),
	@Gender nchar(6),
	@DOB datetime,
	@Address nvarchar(50),
	@Age int=0
AS
BEGIN
	IF @Name = ''
	SET @Name = NULL
	IF @Gender = '0'
	SET @Gender = NULL
	IF @DOB = ''
	SET @DOB = NULL
	IF @Address = ''
	SET @Address = NULL
	IF @Age = 0
	SET @Age = NULL

	SELECT * FROM Patients
	WHERE Name LIKE '%'+ISNULL(@Name,Name)+'%' AND Gender=ISNULL(@Gender,Gender) AND DOB=ISNULL(@DOB,DOB) AND Address LIKE '%'+ISNULL(@Address,Address)+'%'
	AND Age=ISNULL(@Age,Age)
END
RETURN 0
GO

