USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spDeleteDoctor]    Script Date: 9/3/2013 9:02:23 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spDeleteDoctor]
	@ID int=0
AS
	DELETE FROM Doctors WHERE ID=@ID
RETURN 0
GO

