USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateICDGroup]    Script Date: 9/3/2013 9:18:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdateICDGroup]
	@ID int=0,
	@Name nvarchar(MAX)
AS
	UPDATE ICDGroups SET Name=@Name WHERE ID=@ID
RETURN 0
GO

