USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spSearchMedicalServiceGroup]    Script Date: 9/3/2013 9:10:13 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSearchMedicalServiceGroup]	
	@Name nvarchar(50)
AS
BEGIN	
	IF @Name = ''
	SET @Name = NULL

	SELECT * FROM MedicalServiceGroups
	WHERE Name LIKE '%'+ISNULL(@Name,Name)+'%'
END
RETURN 0
GO

