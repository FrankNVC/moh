USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spSearchPrescription]    Script Date: 9/3/2013 9:10:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSearchPrescription]
	@Date date,
	@DID int=0
AS
BEGIN	
	IF @Date = ''
	SET @Date = NULL
	IF @DID = 0
	SET @DID = NULL

	SELECT * FROM Prescriptions
	WHERE Date=ISNULL(@Date,Date) AND DID=ISNULL(@DID,DID)
END
RETURN 0
GO

