USE [ERM]
GO

INSERT INTO [dbo].[Visits]
           ([Date]
           ,[PatientID]
           ,[HospitalID]
           ,[DoctorID]
           ,[PrescriptionID]
           ,[LabOrderID]
           ,[ICDID]
           ,[Outcome])
     VALUES
           ('08-30-2013',
           1,
           1,
           2,
           1,
           1,
           1,
           'CURED')

INSERT INTO [dbo].[Visits]
           ([Date]
           ,[PatientID]
           ,[HospitalID]
           ,[DoctorID]
           ,[PrescriptionID]
           ,[LabOrderID]
           ,[ICDID]
           ,[Outcome])
     VALUES
           ('08-30-2013',
           2,
           2,
           3,
           2,
           2,
           2,
           'DIED')

INSERT INTO [dbo].[Visits]
           ([Date]
           ,[PatientID]
           ,[HospitalID]
           ,[DoctorID]
           ,[PrescriptionID]
           ,[LabOrderID]
           ,[ICDID]
           ,[Outcome])
     VALUES
           ('08-30-2013',
           3,
           2,
           3,
           3,
           3,
           3,
           'UNCHANGED')
GO

