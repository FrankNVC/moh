USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spInsertDrug]    Script Date: 9/3/2013 9:05:57 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spInsertDrug]
	@Name nvarchar(50),
	@Generic nvarchar(50),
	@Unit nvarchar(10),
	@Price money,
	@GroupID int=0
AS
	INSERT INTO Drugs VALUES(@Name,@Generic,@Unit,@Price,@GroupID)
RETURN 0
GO

