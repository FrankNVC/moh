USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spDeleteMedicalServiceGroup]    Script Date: 9/3/2013 9:04:42 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spDeleteMedicalServiceGroup]
	@ID int=0
AS
	DELETE FROM MedicalServiceGroups WHERE ID=@ID
RETURN 0
GO

