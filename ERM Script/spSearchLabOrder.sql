USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spSearchLabOrder]    Script Date: 9/3/2013 9:09:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSearchLabOrder]	
	@Date date,
	@DID int=0
AS
BEGIN	
	IF @Date = ''
	SET @Date = NULL
	IF @DID = 0
	SET @DID = NULL	

	SELECT * FROM LabOrders
	WHERE Date=ISNULL(@Date,Date) AND DID=ISNULL(@DID,DID)
END
RETURN 0
GO

