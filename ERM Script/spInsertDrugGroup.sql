USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spInsertDrugGroup]    Script Date: 9/3/2013 9:06:07 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spInsertDrugGroup]
	@Name nvarchar(50)
AS
	INSERT INTO DrugGroups VALUES (@Name)
RETURN 0
GO

