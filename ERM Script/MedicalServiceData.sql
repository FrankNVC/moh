USE [ERM]
GO

INSERT INTO [dbo].[MedicalServices]
           ([Name]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Allergy Testing',
           13,
           1)

INSERT INTO [dbo].[MedicalServices]
           ([Name]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Breathing Capacity Testing',
           42,
           1)

INSERT INTO [dbo].[MedicalServices]
           ([Name]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Hearing Test',
           96,
           1)

INSERT INTO [dbo].[MedicalServices]
           ([Name]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Muscle Testing',
           111,
           1)

INSERT INTO [dbo].[MedicalServices]
           ([Name]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Brain CT (no contrast)',
           289,
           1)

INSERT INTO [dbo].[MedicalServices]
           ([Name]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Brain CT (with & without contrast)',
           707,
           1)

INSERT INTO [dbo].[MedicalServices]
           ([Name]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Brain MRI (no contrast)',
           560,
           1)

INSERT INTO [dbo].[MedicalServices]
           ([Name]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Chest MRI (no contrast)',
           567,
           1)

INSERT INTO [dbo].[MedicalServices]
           ([Name]
           ,[Price]
           ,[GroupID])
     VALUES
           ('X-Ray Chest',
           44,
           1)

INSERT INTO [dbo].[MedicalServices]
           ([Name]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Hemoglobin (Hgb) A1c',
           44,
           2)

INSERT INTO [dbo].[MedicalServices]
           ([Name]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Breast Cancer Profile',
           180,
           2)

INSERT INTO [dbo].[MedicalServices]
           ([Name]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Insulin, Fasting',
           49,
           2)

INSERT INTO [dbo].[MedicalServices]
           ([Name]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Filling (Amalgam, 1 surface)',
           88,
           3)

INSERT INTO [dbo].[MedicalServices]
           ([Name]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Filling (Amalgam, 2 surfaces)',
           112,
           3)

INSERT INTO [dbo].[MedicalServices]
           ([Name]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Filling (Amalgam, 3 surfaces)',
           133,
           3)

INSERT INTO [dbo].[MedicalServices]
           ([Name]
           ,[Price]
           ,[GroupID])
     VALUES
           ('3/4 Crown ',
           855,
           3)

INSERT INTO [dbo].[MedicalServices]
           ([Name]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Full Crown',
           861,
           3)
INSERT INTO [dbo].[MedicalServices]
           ([Name]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Porcelain Crown ',
           863,
           3)
GO

