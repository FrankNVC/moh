USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spUpdatePrescription]    Script Date: 9/3/2013 9:19:50 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdatePrescription]
	@ID int=0,
	@Date date,
	@DID int=0
AS
	UPDATE Prescriptions SET Date=@Date, DID=@DID WHERE ID=@ID
RETURN 0
GO

