USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spDeletePrescription]    Script Date: 9/3/2013 9:05:05 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spDeletePrescription]
	@ID int=0
AS
	DELETE FROM Prescriptions WHERE ID=@ID
RETURN 0
GO

