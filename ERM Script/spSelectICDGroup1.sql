USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spSelectICDGroup1]    Script Date: 9/3/2013 9:12:46 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSelectICDGroup1]
	@ID int=0
AS
	SELECT * FROM ICDGroups WHERE ID=@ID
RETURN 0
GO

