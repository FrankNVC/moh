USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spSearchHospital]    Script Date: 9/3/2013 9:09:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSearchHospital]
	@Name nvarchar(50),	
	@LicenseNo nvarchar(6),
	@Address nvarchar(50)
AS
BEGIN
	IF @Name = ''
	SET @Name = NULL	
	IF @LicenseNo= ''
	SET @LicenseNo = NULL
	IF @Address= ''
	SET @Address = NULL

	SELECT * FROM Hospitals
	WHERE Name LIKE '%'+ISNULL(@Name,Name)+'%' AND LicenseNo LIKE '%'+ISNULL(@LicenseNo,LicenseNo)+'%' 
	AND Address LIKE '%'+ISNULL(@Address,Address)+'%'
END
RETURN 0
GO

