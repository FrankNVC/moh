USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spSearchICDGroup]    Script Date: 9/3/2013 9:09:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSearchICDGroup]
	@Name nvarchar(MAX)
AS
BEGIN
	IF @Name = ''
	SET @Name = NULL

	SELECT * FROM ICDGroups
	WHERE Name LIKE '%'+ISNULL(@Name,Name)+'%'
END
RETURN 0
GO

