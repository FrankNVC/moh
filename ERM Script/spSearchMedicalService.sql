USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spSearchMedicalService]    Script Date: 9/3/2013 9:10:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSearchMedicalService]	
	@Name nvarchar(50),
	@Price money=0,
	@GroupID int=0
AS
BEGIN	
	IF @Name = ''
	SET @Name = NULL
	IF @Price = 0
	SET @Price = NULL
	IF @GroupID = 0
	SET @GroupID = NULL

	SELECT * FROM MedicalServices
	WHERE Name LIKE '%'+ISNULL(@Name,Name)+'%' AND Price=ISNULL(@Price,Price) AND GroupID=ISNULL(@GroupID,GroupID)
END
RETURN 0
GO

