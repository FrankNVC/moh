USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateHospital]    Script Date: 9/3/2013 9:18:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdateHospital]
	@ID int=0,
	@Name nvarchar(50),
	@LicenseNo nvarchar(6),
	@Address nvarchar(50)
AS
	UPDATE Hospitals SET Name=@Name, LicenseNo=@LicenseNo, Address=@Address WHERE ID=@ID
RETURN 0
GO

