USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spDeletePatient]    Script Date: 9/3/2013 9:04:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spDeletePatient]
	@ID int=0
AS
	DELETE FROM Patients WHERE ID=@ID
RETURN 0
GO

