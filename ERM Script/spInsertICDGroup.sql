USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spInsertICDGroup]    Script Date: 9/3/2013 9:06:46 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spInsertICDGroup]
	@Name nvarchar(MAX)
AS
	INSERT INTO ICDGroups VALUES(@Name)
RETURN 0
GO

