USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spDeletePrescriptionDetail]    Script Date: 9/3/2013 9:05:19 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spDeletePrescriptionDetail]
	@ID int=0
AS
	DELETE FROM PrescriptionDetails WHERE ID=@ID
RETURN 0
GO

