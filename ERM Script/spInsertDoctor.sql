USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spInsertDoctor]    Script Date: 9/3/2013 9:05:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spInsertDoctor]
	@Name nvarchar(50),
	@Gender nchar(6),
	@DOB date,
	@LicenseNo nvarchar(6),
	@Address nvarchar(50)
AS
	INSERT INTO Doctors VALUES(@Name,@Gender,@DOB,@LicenseNo,@Address)
RETURN 0
GO

