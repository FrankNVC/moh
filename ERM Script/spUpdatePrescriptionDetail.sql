USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spUpdatePrescriptionDetail]    Script Date: 9/3/2013 9:20:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdatePrescriptionDetail]
	@ID int=0,
	@DrugID int=0,
	@PrescriptionID int=0,
	@Quantity int=0,
	@Dose int=0,
	@Instruction nvarchar(50)
AS
	UPDATE PrescriptionDetails SET DrugID=@DrugID, PrescriptionID=@PrescriptionID, Quantity=@Quantity, Dose=@Dose, Instruction=@Instruction WHERE ID=@ID
RETURN 0
GO

