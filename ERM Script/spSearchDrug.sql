USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spSearchDrug]    Script Date: 9/3/2013 9:08:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSearchDrug]
	@Name nvarchar(50),
	@Generic nvarchar(50),
	@Unit nvarchar(10),
	@Price money=0,
	@GroupID int=0
AS
BEGIN
	IF @Name = ''
	SET @Name = NULL
	IF @Generic = ''
	SET @Generic = NULL
	IF @Unit = ''
	SET @Unit = NULL
	IF @Price = 0
	SET @Price = NULL
	IF @GroupID = 0
	SET @GroupID = NULL

	SELECT * FROM Drugs
	WHERE Name LIKE '%'+ISNULL(@Name,Name)+'%' AND Generic LIKE '%'+ISNULL(@Generic,Generic)+'%'
	AND Unit LIKE '%'+ISNULL(@Unit,Unit)+'%' AND Price=ISNULL(@Price,Price) AND GroupID=ISNULL(@GroupID,GroupID)
END
RETURN 0
GO

