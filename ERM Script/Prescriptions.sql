USE [ERM]
GO

/****** Object:  Table [dbo].[Prescriptions]    Script Date: 9/3/2013 7:58:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Prescriptions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Date] [date] NOT NULL,
	[DID] [int] NOT NULL,
 CONSTRAINT [PK_Prescriptions2] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Prescriptions]  WITH CHECK ADD  CONSTRAINT [FK_Prescriptions2_Doctors2] FOREIGN KEY([DID])
REFERENCES [dbo].[Doctors] ([ID])
GO

ALTER TABLE [dbo].[Prescriptions] CHECK CONSTRAINT [FK_Prescriptions2_Doctors2]
GO

