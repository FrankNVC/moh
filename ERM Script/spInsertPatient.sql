USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spInsertPatient]    Script Date: 9/3/2013 8:07:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spInsertPatient]
	--@ID int=0,
	@Name nvarchar(50),
	@Gender nchar(6),
	@DOB datetime,
	@Address nvarchar(50),
	@Age int=0
AS
	INSERT INTO Patients VALUES(@Name,@Gender,@DOB,@Address,@Age)
RETURN 0
GO

