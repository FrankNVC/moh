USE [ERM]
GO

INSERT INTO [dbo].[DrugGroups]
           ([Name])
     VALUES
           ('Herbal Products')

INSERT INTO [dbo].[DrugGroups]
           ([Name])
     VALUES
           ('Nutraceutical products')

INSERT INTO [dbo].[DrugGroups]
           ([Name])
     VALUES
           ('Probiotics')

INSERT INTO [dbo].[DrugGroups]
           ([Name])
     VALUES
           ('Azole Antifungals')

INSERT INTO [dbo].[DrugGroups]
           ([Name])
     VALUES
           ('Antiviral Combinations')

INSERT INTO [dbo].[DrugGroups]
           ([Name])
     VALUES
           ('Glycopeptide Antibiotics')

INSERT INTO [dbo].[DrugGroups]
           ([Name])
     VALUES
           ('Glycylcyclines')

INSERT INTO [dbo].[DrugGroups]
           ([Name])
     VALUES
           ('Aminopenicillins')

INSERT INTO [dbo].[DrugGroups]
           ([Name])
     VALUES
           ('Beta-lactamase Inhibitors')

INSERT INTO [dbo].[DrugGroups]
           ([Name])
     VALUES
           ('Natural Penicillins')

INSERT INTO [dbo].[DrugGroups]
           ([Name])
     VALUES
           ('Antiandrogen')
GO

