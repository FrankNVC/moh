USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spSelectICD1]    Script Date: 9/3/2013 9:12:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSelectICD1]
	@ID int=0
AS
	SELECT * FROM ICDs WHERE ID=@ID
RETURN 0
GO

