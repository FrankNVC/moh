USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spSearchDoctor]    Script Date: 9/3/2013 9:08:42 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSearchDoctor]
	@Name nvarchar(50),
	@Gender nchar(6),
	@DOB date,
	@LicenseNo nvarchar(6),
	@Address nvarchar(50)
AS
BEGIN
	IF @Name = ''
	SET @Name = NULL
	IF @Gender = '0'
	SET @Gender = NULL
	IF @DOB= ''
	SET @DOB = NULL
	IF @LicenseNo= ''
	SET @LicenseNo = NULL
	IF @Address= ''
	SET @Address = NULL

	SELECT * FROM Doctors
	WHERE Name LIKE '%'+ISNULL(@Name,Name)+'%' AND Gender=ISNULL(@Gender,Gender) AND DOB=ISNULL(@DOB,DOB)
	AND LicenseNo LIKE '%'+ISNULL(@LicenseNo,LicenseNo)+'%' AND Address LIKE '%'+ISNULL(@Address,Address)+'%'
END
RETURN 0
GO

