USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateMedicalService]    Script Date: 9/3/2013 9:19:22 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdateMedicalService]
	@ID int=0,
	@Name nvarchar(50),
	@Price money,
	@GroupID int=0
AS
	UPDATE MedicalServices SET Name=@Name, Price=@Price, GroupID=@GroupID WHERE ID=@ID
RETURN 0
GO

