USE [ERM]
GO

INSERT INTO [dbo].[LabOrderDetails]
           ([MedicalServiceID]
           ,[Result]
           ,[LabOrderID])
     VALUES
           (1,'DONE',1)

INSERT INTO [dbo].[LabOrderDetails]
           ([MedicalServiceID]
           ,[Result]
           ,[LabOrderID])
     VALUES
           (2,'DONE',2)

INSERT INTO [dbo].[LabOrderDetails]
           ([MedicalServiceID]
           ,[Result]
           ,[LabOrderID])
     VALUES
           (20,'DONE',1)

INSERT INTO [dbo].[LabOrderDetails]
           ([MedicalServiceID]
           ,[Result]
           ,[LabOrderID])
     VALUES
           (15,'DONE',3)
GO

