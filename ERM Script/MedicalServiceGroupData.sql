USE [ERM]
GO

INSERT INTO [dbo].[MedicalServiceGroups]
           ([Name])
     VALUES
           ('Physician')

INSERT INTO [dbo].[MedicalServiceGroups]
           ([Name])
     VALUES
           ('Laboratory')

INSERT INTO [dbo].[MedicalServiceGroups]
           ([Name])
     VALUES
           ('Dental')
GO

