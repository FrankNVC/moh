USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spDeleteHospital]    Script Date: 9/3/2013 9:03:13 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spDeleteHospital]
	@ID int=0
AS
	DELETE FROM Hospitals WHERE ID=@ID
RETURN 0
GO

