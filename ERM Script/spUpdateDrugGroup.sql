USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateDrugGroup]    Script Date: 9/3/2013 9:18:28 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdateDrugGroup]
	@ID int=0,
	@Name nvarchar(50)
AS
	UPDATE DrugGroups SET Name=@Name WHERE ID=@ID
RETURN 0
GO

