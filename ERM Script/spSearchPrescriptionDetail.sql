USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spSearchPrescriptionDetail]    Script Date: 9/3/2013 9:10:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSearchPrescriptionDetail]
	@DrugID int=0,
	@PrescriptionID int=0,
	@Quantity int=0,
	@Dose int=0
AS
BEGIN
	IF @DrugID = 0
	SET @DrugID = NULL
	IF @PrescriptionID = 0
	SET @PrescriptionID = NULL
	IF @Quantity = 0
	SET @Quantity = NULL
	IF @Dose = 0
	SET @Dose = NULL

	SELECT * FROM PrescriptionDetails
	WHERE DrugID=ISNULL(@DrugID,DrugID) AND PrescriptionID=ISNULL(@PrescriptionID,PrescriptionID) AND Quantity=ISNULL(@Quantity,Quantity)
	AND Dose=ISNULL(@Dose,Dose)
END
RETURN 0
GO

