USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spInsertICD]    Script Date: 9/3/2013 9:06:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spInsertICD]
	@Chapter nvarchar(MAX),
	@Name nvarchar(MAX),
	@Code nvarchar(5),
	@GroupID int=0
AS
	INSERT INTO ICDs VALUES(@Chapter,@Name,@Code,@GroupID)
RETURN 0
GO

