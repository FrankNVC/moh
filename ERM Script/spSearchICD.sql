USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spSearchICD]    Script Date: 9/3/2013 9:09:20 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSearchICD]
	@Chapter nvarchar(MAX),
	@Name nvarchar(MAX),
	@Code nvarchar(5),
	@GroupID int=0
AS
BEGIN
	IF @Chapter = ''
	SET @Chapter = NULL
	IF @Name = ''
	SET @Name = NULL
	IF @Code = ''
	SET @Code = NULL
	IF @GroupID = 0
	SET @GroupID = NULL

	SELECT * FROM ICDs
	WHERE Chapter LIKE '%'+ISNULL(@Chapter,Chapter)+'%' AND Name LIKE '%'+ISNULL(@Name,Name)+'%'
	AND Code LIKE '%'+ISNULL(@Code,Code)+'%' AND GroupID=ISNULL(@GroupID,GroupID)
END
RETURN 0
GO

