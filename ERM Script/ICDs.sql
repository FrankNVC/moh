USE [ERM]
GO

/****** Object:  Table [dbo].[ICDs]    Script Date: 9/3/2013 7:57:09 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ICDs](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Chapter] [nvarchar](max) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Code] [nvarchar](5) NOT NULL,
	[GroupID] [int] NOT NULL,
 CONSTRAINT [PK_ICDs2_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_ICDs2] UNIQUE NONCLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[ICDs]  WITH CHECK ADD  CONSTRAINT [FK_ICDs2_ICDGroups2] FOREIGN KEY([GroupID])
REFERENCES [dbo].[ICDGroups] ([ID])
GO

ALTER TABLE [dbo].[ICDs] CHECK CONSTRAINT [FK_ICDs2_ICDGroups2]
GO

