USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spInsertDoctor2]    Script Date: 9/3/2013 9:05:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spInsertDoctor2]
	@Name varchar(50),
	@Gender nchar(6),
	@DOB date,
	@LicenseNo nvarchar(6),
	@Address nvarchar(50)
AS
BEGIN 
    IF  NOT EXISTS(SELECT (1) FROM Doctors WHERE LicenseNo = @LicenseNo )
        BEGIN
            INSERT INTO Doctors(Name,Gender,DOB,LicenseNo,Address) VALUES(@Name,@Gender,@DOB,@LicenseNo,@Address)
        END 
END
GO

