USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spSearchVisit]    Script Date: 9/3/2013 9:11:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSearchVisit]
	@Date date,
	@PatientID int=0,
	@HospitalID int=0,
	@DoctorID int=0,
	@PrescriptionID int=0,
	@LabOrderID int=0,
	@ICDID int=0,
	@Outcome nvarchar(10)
AS
BEGIN	
	IF @Date = ''
	SET @Date = NULL
	IF @PatientID = 0
	SET @PatientID = NULL
	IF @HospitalID = 0
	SET @HospitalID = NULL
	IF @DoctorID = 0
	SET @DoctorID = NULL
	IF @PrescriptionID = 0
	SET @PrescriptionID = NULL
	IF @LabOrderID = 0
	SET @LabOrderID = NULL
	IF @ICDID = 0
	SET @ICDID = NULL
	IF @Outcome = '0'
	SET @Outcome = NULL

	SELECT * FROM Visits
	WHERE Date=ISNULL(@Date,Date) AND PatientID=ISNULL(@PatientID,PatientID) AND HospitalID=ISNULL(@HospitalID,HospitalID)
	AND DoctorID=ISNULL(@DoctorID,DoctorID) AND PrescriptionID=ISNULL(@PrescriptionID,PrescriptionID) AND LabOrderID=ISNULL(@LabOrderID,LabOrderID)
	AND ICDID=ISNULL(@ICDID,ICDID) AND Outcome LIKE '%'+ISNULL(@Outcome,Outcome)+'%'
END
RETURN 0
GO

