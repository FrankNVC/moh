USE [ERM]
GO

INSERT INTO [dbo].[ICDs]
           ([Chapter]
           ,[Name]
           ,[Code]
           ,[GroupID])
     VALUES
           ('Certain infectious and parasitic diseases',
           'Cholera due to Vibrio cholerae 01, biovar cholerae',
           'A00.0',
           1)

INSERT INTO [dbo].[ICDs]
           ([Chapter]
           ,[Name]
           ,[Code]
           ,[GroupID])
     VALUES
		   ('Certain infectious and parasitic diseases',
		   'Paratyphoid fever A',
		   'A01.1',
		   1)

INSERT INTO [dbo].[ICDs]
           ([Chapter]
           ,[Name]
           ,[Code]
           ,[GroupID])
     VALUES
	       ('Certain infectious and parasitic diseases',
		   'Hepatitis A with hepatic coma',
		   'B15.0',
		   1)

INSERT INTO [dbo].[ICDs]
           ([Chapter]
           ,[Name]
           ,[Code]
           ,[GroupID])
     VALUES
		   ('Neoplasms',
		   'Dorsal surface of tongue',
		   'C02.0',
		   22)
GO

