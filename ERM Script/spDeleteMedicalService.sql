USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spDeleteMedicalService]    Script Date: 9/3/2013 9:04:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spDeleteMedicalService]
	@ID int=0
AS
	DELETE FROM MedicalServices WHERE ID=@ID
RETURN 0
GO

