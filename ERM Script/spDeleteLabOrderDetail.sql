USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spDeleteLabOrderDetail]    Script Date: 9/3/2013 9:04:01 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spDeleteLabOrderDetail]
	@ID int=0
AS
	DELETE FROM LabOrderDetails WHERE ID=@ID
RETURN 0
GO

