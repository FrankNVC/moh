USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spSearchLabOrderDetail]    Script Date: 9/3/2013 9:09:50 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSearchLabOrderDetail]
	@MedicalServiceID int=0,
	@Result nvarchar(10),
	@LabOrderID int=0
AS
BEGIN
	IF @MedicalServiceID = 0
	SET @MedicalServiceID = NULL
	IF @Result = ''
	SET @Result = NULL
	IF @LabOrderID = 0
	SET @LabOrderID = NULL	

	SELECT * FROM LabOrderDetails
	WHERE MedicalServiceID=ISNULL(@MedicalServiceID,MedicalServiceID) AND Result LIKE '%'+ISNULL(@Result,Result)+'%' AND LabOrderID=ISNULL(@LabOrderID,LabOrderID)
END
RETURN 0
GO

