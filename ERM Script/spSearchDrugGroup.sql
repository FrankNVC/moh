USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spSearchDrugGroup]    Script Date: 9/3/2013 9:09:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSearchDrugGroup]
	@Name nvarchar(50)
AS
BEGIN
	IF @Name = ''
	SET @Name = NULL	

	SELECT * FROM DrugGroups
	WHERE Name LIKE '%'+ISNULL(@Name,Name)+'%'
END
RETURN 0
GO

