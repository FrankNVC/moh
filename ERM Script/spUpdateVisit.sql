USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateVisit]    Script Date: 9/3/2013 9:20:10 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdateVisit]
	@ID int=0,
	@Date date,
	@PatientID int=0,
	@HospitalID int=0,
	@DoctorID int=0,
	@PrescriptionID int=0,
	@LabOrderID int=0,
	@ICDID int=0,
	@Outcome nvarchar(10)
AS
	UPDATE Visits SET Date=@Date, PatientID=@PatientID, HospitalID=@HospitalID, DoctorID=@DoctorID, PrescriptionID=@PrescriptionID, LabOrderID=@LabOrderID, ICDID=@ICDID, Outcome=@Outcome WHERE ID=@ID
RETURN 0
GO

