USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spSelectLabOrderDetail2]    Script Date: 9/3/2013 9:14:58 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSelectLabOrderDetail2]
	@LabOrderID int=0
AS
	SELECT * FROM LabOrderDetails WHERE LabOrderID=@LabOrderID
RETURN 0
GO

