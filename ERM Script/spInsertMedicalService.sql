USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spInsertMedicalService]    Script Date: 9/3/2013 9:07:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spInsertMedicalService]
	
	@Name nvarchar(50),
	@Price money,
	@GroupID int=0
AS
	INSERT INTO MedicalServices VALUES(@Name,@Price,@GroupID)
RETURN 0
GO

