USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spDeleteVisit]    Script Date: 9/3/2013 9:05:29 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spDeleteVisit]
	@ID int=0
AS
	DELETE FROM Visits WHERE ID=@ID
RETURN 0
GO

