USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateICD]    Script Date: 9/3/2013 9:18:42 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdateICD]
	@ID int=0,
	@Chapter nvarchar(MAX),
	@Name nvarchar(MAX),
	@Code nvarchar(5),
	@GroupID int=0
AS
	UPDATE ICDs SET Chapter=@Chapter, Name=@Name, Code=@Code, GroupID=@GroupID WHERE ID=@ID
RETURN 0
GO

