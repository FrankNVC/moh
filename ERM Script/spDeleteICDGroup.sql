USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spDeleteICDGroup]    Script Date: 9/3/2013 9:03:33 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spDeleteICDGroup]
	@ID int=0
AS
	DELETE FROM ICDGroups WHERE ID=@ID
RETURN 0
GO

