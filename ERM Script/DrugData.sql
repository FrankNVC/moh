USE [ERM]
GO

INSERT INTO [dbo].[Drugs]
           ([Name]
           ,[Generic]
           ,[Unit]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Azo-Cranberry',
           'cranberry',
           'Tablet',
           50,
           1)

INSERT INTO [dbo].[Drugs]
           ([Name]
           ,[Generic]
           ,[Unit]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Menopause Support',
           'black cohosh',
           'Pill',
           30,
           1)

INSERT INTO [dbo].[Drugs]
           ([Name]
           ,[Generic]
           ,[Unit]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Primrose Oil',
           'evening primrose',
           'Bottle',
           50,
           1)

INSERT INTO [dbo].[Drugs]
           ([Name]
           ,[Generic]
           ,[Unit]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Sporanox',
           'itraconzaloe',
           'Capsule',
           25,
           4)

INSERT INTO [dbo].[Drugs]
           ([Name]
           ,[Generic]
           ,[Unit]
           ,[Price]
           ,[GroupID])
     VALUES
           ('Pen-V',
           'penicillin V',
           'Bottle',
           35,
           10)
GO

