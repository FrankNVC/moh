USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spInsertLabOrder]    Script Date: 9/3/2013 9:06:59 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spInsertLabOrder]
	
	@Date date,
	@DID int=0
AS
	INSERT INTO LabOrders VALUES (@Date,@DID)
RETURN 0
GO

