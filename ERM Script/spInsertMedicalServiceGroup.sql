USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spInsertMedicalServiceGroup]    Script Date: 9/3/2013 9:07:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spInsertMedicalServiceGroup]
	
	@Name nvarchar(50)
AS
	INSERT INTO MedicalServiceGroups VALUES (@Name)
RETURN 0
GO

