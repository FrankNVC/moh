USE [ERM]
GO

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Intestinal infectious diseases')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Tuberculosis')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Certain zoonotic bacterial diseases')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Other bacterial diseases')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Infections with a predominantly sexual mode of transmission')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Other spirochaetal diseases')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Other diseases caused by chlamydiae')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Rickettsioses')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Viral infections of the central nervous system')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Arthropod-borne viral fevers and viral haemorrhagic fevers')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Viral infections characterized by skin and mucous membrane lesions')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Viral hepatitis')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Human immunodeficiency virus [HIV] disease')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Other viral diseases')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Mycoses')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Protozoal diseases')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Helminthiases')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Pediculosis, acariasis and other infestations')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Sequelae of infectious and parasitic diseases')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Bacterial, viral and other infectious agents')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Other infectious diseases')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Malignant neoplasms')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('In situ neoplasms')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Benign neoplasms')

INSERT INTO [dbo].[ICDGroups]
           ([Name])
     VALUES
           ('Neoplasms of uncertain or unknown behaviour')
GO

