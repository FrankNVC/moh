USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spUpdateDoctor]    Script Date: 9/3/2013 9:18:12 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdateDoctor]
	@ID int=0,
	@Name nvarchar(50),
	@Gender nchar(6),
	@DOB date,
	@LicenseNo nchar(6),
	@Address nvarchar(50)
AS
	UPDATE Doctors SET Name=@Name, Gender=@Gender, DOB=@DOB, LicenseNo=@LicenseNo, Address=@Address WHERE ID=@ID
RETURN 0
GO

