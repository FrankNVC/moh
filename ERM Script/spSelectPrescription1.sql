USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spSelectPrescription1]    Script Date: 9/3/2013 9:16:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSelectPrescription1]
	@ID int=0

AS
	SELECT * FROM Prescriptions WHERE ID=@ID
RETURN 0
GO

