USE [ERM]
GO

/****** Object:  Table [dbo].[MedicalServices]    Script Date: 9/3/2013 7:58:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MedicalServices](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Price] [money] NOT NULL,
	[GroupID] [int] NOT NULL,
 CONSTRAINT [PK_MedicalServices2] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_MedicalServices2] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MedicalServices]  WITH CHECK ADD  CONSTRAINT [FK_MedicalServices2_MedicalServiceGroups2] FOREIGN KEY([GroupID])
REFERENCES [dbo].[MedicalServiceGroups] ([ID])
GO

ALTER TABLE [dbo].[MedicalServices] CHECK CONSTRAINT [FK_MedicalServices2_MedicalServiceGroups2]
GO

