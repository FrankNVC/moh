USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spInsertPrescriptionDetail]    Script Date: 9/3/2013 9:08:09 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spInsertPrescriptionDetail]
	@DrugID int=0,
	@PrescriptionID int=0,
	@Quantity int=0,
	@Dose int=0,
	@Instruction nvarchar(50)
AS
	INSERT INTO PrescriptionDetails VALUES(@DrugID,@PrescriptionID,@Quantity,@Dose,@Instruction)
RETURN 0
GO

