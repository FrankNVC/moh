USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spInsertHospital2]    Script Date: 9/3/2013 9:06:27 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spInsertHospital2]
	@Name varchar(50),
	@LicenseNo nvarchar(6),
	@Address nvarchar(50)
AS
BEGIN 
    IF  NOT EXISTS(SELECT (1) FROM Hospitals WHERE LicenseNo = @LicenseNo )
        BEGIN
            INSERT INTO Hospitals(Name,LicenseNo,Address) VALUES(@Name,@LicenseNo,@Address)
        END 
END
GO

