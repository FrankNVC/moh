USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spUpdatePatient]    Script Date: 9/3/2013 8:07:17 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdatePatient]
	@ID int=0,
	@Name nvarchar(50),
	@Gender nchar(6),
	@DOB datetime,
	@Address nvarchar(50),
	@Age int=0
AS
	UPDATE Patients SET Name=@Name, Gender=@Gender, DOB=@DOB, Address=@Address, Age=ISNULL(@Age,Age) WHERE ID=@ID
RETURN 0
GO

