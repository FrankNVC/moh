USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spDeleteDrug]    Script Date: 9/3/2013 9:02:44 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spDeleteDrug]
	@ID int=0
AS
	DELETE FROM Drugs WHERE ID=@ID
RETURN 0
GO

