USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spDeleteLabOrder]    Script Date: 9/3/2013 9:03:46 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spDeleteLabOrder]
	@ID int=0
AS
	DELETE FROM LabOrders WHERE ID=@ID
RETURN 0
GO

