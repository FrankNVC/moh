USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spDeleteICD]    Script Date: 9/3/2013 9:03:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spDeleteICD]
	
	@ID int=0
AS
	DELETE FROM ICDs WHERE ID=@ID
RETURN 0
GO

