USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spInsertHospital]    Script Date: 9/3/2013 9:06:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spInsertHospital]
	@Name nvarchar(50),
	@LicenseNo nvarchar(6),
	@Address nvarchar(50)
AS
	INSERT INTO Hospitals VALUES(@Name,@LicenseNo,@Address)
RETURN 0
GO

