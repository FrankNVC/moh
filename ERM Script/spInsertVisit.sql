USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spInsertVisit]    Script Date: 9/3/2013 9:08:20 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spInsertVisit]
	@Date date,
	@PatientID int=0,
	@HospitalID int=0,
	@DoctorID int=0,
	@PrescriptionID int=0,
	@LabOrderID int=0,
	@ICDID int=0,
	@Outcome nvarchar(10)
AS
	INSERT INTO Visits VALUES (@Date,@PatientID,@HospitalID,@DoctorID,@PrescriptionID,@LabOrderID,@ICDID,@Outcome)
RETURN 0
GO

