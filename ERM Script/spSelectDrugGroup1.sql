USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spSelectDrugGroup1]    Script Date: 9/3/2013 9:11:45 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spSelectDrugGroup1]
	@ID int=0
AS
	SELECT * FROM DrugGroups WHERE ID=@ID
RETURN 0
GO

