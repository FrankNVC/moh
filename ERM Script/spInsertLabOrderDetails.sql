USE [ERM]
GO

/****** Object:  StoredProcedure [dbo].[spInsertLabOrderDetail]    Script Date: 9/3/2013 9:07:13 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spInsertLabOrderDetail]
	@MedicalServiceID int=0,
	@Result nvarchar(10),
	@LabOrderID int=0
AS
	INSERT INTO LabOrderDetails VALUES(@MedicalServiceID,@Result,@LabOrderID)
RETURN 0
GO

